import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kobdostu/static/sharedpref.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/login.dart';
import 'package:kobdostu/views/profile.dart';
import 'package:kobdostu/views/search.dart';

import 'notificationbox.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  int count = 0;
  Timer timer;
  @override
  void initState() {
    super.initState();

    firebaseCloudMessagingListeners();
    StaticValue.fcmStream = _firebaseMessaging.onTokenRefresh;
    StaticValue.fcmStream.listen((token) {
      if(!token.contains("Token retrieval failed")){
      print("Firebase device token:  " + token ?? "notoken");
      StaticValue.UpdateConnection(token);
      StaticValue.notifyToken = token;
      return;
      }
      StaticValue.notifyToken="notoken";
    });
  }

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  showNotificationWithDefaultSound(Map<String, dynamic> msg) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      msg['title'],
      msg['body'],
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }

  firebaseCloudMessagingListeners() {
    if (Platform.isIOS) iOSPermission();

    try {
      _firebaseMessaging.getToken().then((token) {
      if(token!=null){
       if(!token.contains("Token retrieval failed")){
      print("Firebase device token:  " + token ?? "notoken");
      StaticValue.notifyToken = token;
      return;
      }
      }
      StaticValue.notifyToken="notoken";
    });
    } catch (e) {
      StaticValue.notifyToken="notoken";
    }

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
         try {
       StaticValue.notifyBox.state.countIncrement();
            showNotificationWithDefaultSound(message);
         } catch (e) {
         }
             print("sdsdsdsd---------");          
          print(message);
          
        
        if (StaticValue.accessToken.length != null) {}
        if (message["body"] == "hello") {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => SearchPage(),
              ));
        }
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   super.didChangeAppLifecycleState(state);
  //   switch (state) {
  //     case AppLifecycleState.paused:
  //       print("paused");
  //       break;
  //     case AppLifecycleState.resumed:
  //       print("resumed");
  //       break;
  //     case AppLifecycleState.inactive:
  //       print("inactive");
  //       break;
  //     case AppLifecycleState.suspending:
  //       print("suspended");
  //       break;
  //     default:
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    if (count == 0) {
      
      timer = Timer(Duration(seconds: 2), () {
        SharedPref.getV("accesstoken").then((value) => {
              print("access" + value),
              if (value != null && value != "null")
                {
                  StaticValue.accessToken = value,
                  SharedPref.getV("refreshtoken").then((refreshtoken) => {
                        StaticValue.refreshToken =
                            refreshtoken != null && refreshtoken != "null"
                                ? refreshtoken
                                : null,
                        SharedPref.getV("phone").then((phone) => {
                              StaticValue.phone =
                                  phone != null && phone != "null"
                                      ? phone
                                      : null,
                              SharedPref.getV("region").then((region) => {
                                    StaticValue.region =
                                        region != null && region != "null"
                                            ? region
                                            : null,
                                    SharedPref.getV("address")
                                        .then((address) => {
                                              StaticValue.address =
                                                  address != null &&
                                                          address != "null"
                                                      ? address
                                                      : null,
                                              SharedPref.getV("fullname")
                                                  .then((fullname) => {
                                                        StaticValue.fullname =
                                                            fullname != null &&
                                                                    fullname !=
                                                                        "null"
                                                                ? fullname
                                                                : null,
                                                        SharedPref.getV("email")
                                                            .then((email) => {
                                                                  StaticValue
                                                                      .email = email !=
                                                                              null &&
                                                                          email !=
                                                                              "null"
                                                                      ? email
                                                                      : null,
                                                                  SharedPref.getV(
                                                                          "photo")
                                                                      .then(
                                                                          (photo) =>
                                                                              {
                                                                                StaticValue.photo = photo != null && photo != "null" ? photo : null,
                                                                                Navigator.pushReplacement(
                                                                                  context,
                                                                                  MaterialPageRoute(builder: (context) => ProfilePage()),
                                                                                ),
                                                                              }),
                                                                }),
                                                      }),
                                            }),
                                  }),
                            })
                      }),
                }
              else
                {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  ),
                }
            });
      });

      count++;
    }
    var logo = SvgPicture.asset("assets/logos/koblogo.svg",
        semanticsLabel: 'Kob Logo',
        height: MediaQuery.of(context).size.height * 0.3);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logos/bothkoblogo.png"),
          ),
          color: StaticValue.mainColor,
        ),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: logo,
          ),
        ),
      ),
    );
  }
}

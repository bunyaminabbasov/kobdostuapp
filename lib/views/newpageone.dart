import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kobdostu/behavior/behaviors.dart';
import 'package:kobdostu/models/muraciet.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';
import 'package:kobdostu/views/search.dart';
import 'package:kobdostu/views/uselesspageone.dart';
import 'package:http/http.dart'as http;

import 'muracietlistitem.dart';

class PageOne extends StatefulWidget {
  @override
  _PageOneState createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {
List<MuracietListItem> muraciets= new List<MuracietListItem>();
List<MuracietListItem> _muraciets= new List<MuracietListItem>();
Future update() async {
  
    String url =
        StaticValue.rootEndpoint + "/api/mobile/get/self-request-list";
    try {
      var response = await http.get(
        url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      print(response.statusCode);
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          var _muraciets = (json.decode(response.body));
          print(_muraciets);
          setState(() {
           for (int i = 0; i < _muraciets.length; i++) {
            
            muraciets.add(
              MuracietListItem(
                Muraciet.json(_muraciets[i])
              ),
            );
          } 
          }
          );

        
        }
      }
    } catch (e) {print(e);}
  }

@override
  void initState() {
    
    update().then((e){


    });
    super.initState();
  }
  
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();
  TextStyle idleStyle = TextStyle(
    color: Colors.black54,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
  );
  TextStyle goldStyle = TextStyle(
    color: StaticValue.mainColor,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    decorationThickness: 2.0,
  );
  int choiceState = 1;
  @override
  Widget build(BuildContext context) {
    if(choiceState==1){
      _muraciets.clear();
      muraciets.forEach((m){if(m.muraciet.muracietStatus==1){_muraciets.add(m);}});
    }
    else if(choiceState==2){
      _muraciets.clear();
      muraciets.forEach((m){if(m.muraciet.muracietStatus==0){_muraciets.add(m);}});
    }
    else if(choiceState==3){
      _muraciets.clear();
      muraciets.forEach((m){_muraciets.add(m);});
    }
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        drawer: KobDrawer(_scaffoldKey,context),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Container(
                color: Color.fromRGBO(237, 237, 237, 1),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.23,
              ),
              HeaderView(_scaffoldKey),
              Container(
                height: MediaQuery.of(context).size.height * 0.12,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.11,
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              print("Hello");
                              setState(() {
                                choiceState = 1;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 1
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                "ACIQ",
                                style: choiceState == 1 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              print("World");
                              setState(() {
                                choiceState = 2;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 2
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                "BAĞLI",
                                style: choiceState == 2 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                choiceState = 3;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 3
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                "HAMISI",
                                style: choiceState == 3 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width*0.05,
                  left: MediaQuery.of(context).size.width*0.05,
                  top: MediaQuery.of(context).size.height * 0.11+MediaQuery.of(context).size.height * 0.11,
                ),
                child: muraciets.length > 0
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child:ScrollConfiguration(behavior:NoEffectScrollBehavior(), 
                child: ListView.builder(
                  itemBuilder: (context, position) {
                    if(_muraciets.length>position){
                    switch(choiceState){
                      case 1:
                       return (_muraciets[position]);
                      break;
                      case 2:
                      break;
                    }
                  }
                  },
                  itemCount: muraciets.length,
                ),
              ))
            : Center(
                child: Text(
                  "Bildiriş yooxdur",
                  style: TextStyle(
                      color: Colors.black54,
                      fontFamily: "Montserrat",
                      fontSize: 14),
                ),
              ),
              )
              ,
              Align(
                alignment: Alignment(0.9, 1),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(5),
                        child: FloatingActionButton(
                          heroTag: "srch",
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SearchPage()),
                            );
                          },
                          backgroundColor: Color.fromRGBO(253, 251, 244, 1),
                          child: Icon(
                            Icons.search,
                            color: Colors.black,
                            size: MediaQuery.of(context).size.height * 0.04,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(5),
                        child: FloatingActionButton(
                        heroTag: "add",
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UselessPageOne()),
                          );
                        },
                        backgroundColor: StaticValue.mainColor,
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: MediaQuery.of(context).size.height * 0.04,
                        ),
                      ),),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

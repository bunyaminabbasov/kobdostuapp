import 'package:flutter/material.dart';
import 'package:kobdostu/behavior/behaviors.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';
import 'package:kobdostu/views/page1.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();
  final Widget flower =  Image.asset(
    "assets/logos/flower.png",
    fit: BoxFit.cover,
  );
  String txt = "Seçim edin";
  bool isPressed = false;
  Widget wg = Container();
  Widget iWG = Icon(
    Icons.arrow_drop_down,
    size: 35,
    color: Colors.black45,
  );
  Widget iWG1 = Icon(
    Icons.arrow_drop_up,
    size: 35,
    color: Colors.black45,
  );
  int choice = 0;
  TextEditingController searchController =  TextEditingController();
  @override
  Widget build(BuildContext context) {
    if (choice == 1) {
      txt = "Fin Nömrə ilə ";
    } else if (choice == 2) {
      txt = "Vöen ilə";
    } else if (choice == 3) {
      txt = "Müraciyyət ilə";
    }
    if (!isPressed && choice > 0) {
      wg = Align(
        alignment: Alignment(0, -0.35),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            boxShadow: [
               BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 3,
                  offset: Offset(0, 2))
            ],
          ),
          margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.1,
          ),
          child: Material(
            borderRadius: BorderRadius.circular(6),
            color: Colors.white,
            child: TextField(
              controller: searchController,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
                fontFamily: "Montserrat"
              ),
              decoration: InputDecoration(
                hintText: "Köməkçi söz",
                hintStyle: TextStyle(fontSize: 16,fontFamily: "Montserrat"),
                fillColor: Colors.white,
                prefix: Icon(
                  Icons.ac_unit,
                  color: Colors.transparent,
                  size: 10,
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
            ),
          ),
        ),
      );
    } else if (isPressed) {
      wg = Align(
        child: Container(
            height: MediaQuery.of(context).size.height * 0.2,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6),
              boxShadow: [
                 BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 3,
                    offset: Offset(0, 2))
              ],
            ),
            margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
            ),
            child: ScrollConfiguration(
              behavior: NoEffectScrollBehavior(),
              child: ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: choice == 1
                        ? Color.fromRGBO(137, 137, 137, 0.5)
                        : Colors.transparent,
                  ),
                  child: ListTile(
                    onTap: (() => {
                          setState(() {
                            choice = 1;
                            isPressed = false;
                          })
                        }),
                    title: Text(
                      "Fin Nömrə ilə ",
                      style: TextStyle(
                          color: choice == 1 ? Colors.black : Colors.black45,fontFamily: "Montserrat",fontSize: 16),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: choice == 2
                        ? Color.fromRGBO(137, 137, 137, 0.5)
                        : Colors.transparent,
                  ),
                  child: ListTile(
                    onTap: (() => {
                          setState(() {
                            choice = 2;
                            isPressed = false;
                          })
                        }),
                    title: Text(
                      "Vöen ilə ",
                      style: TextStyle(
                          color: choice == 2 ? Colors.black : Colors.black45,fontFamily: "Montserrat",fontSize: 16),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: choice == 3
                        ? Color.fromRGBO(137, 137, 137, 0.5)
                        : Colors.transparent,
                  ),
                  child: ListTile(
                    onTap: (() => {
                          setState(() {
                            choice = 3;
                            isPressed = false;
                          })
                        }),
                    title: Text(
                      "Müraciyyət ilə ",
                      style: TextStyle(
                          color: choice == 3 ? Colors.black : Colors.black45,fontFamily: "Montserrat",fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),),),
        alignment: Alignment(0, -0.3),
      );
    } else {
      wg = Container();
    }
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        drawer: KobDrawer(_scaffoldKey,context),
        key: _scaffoldKey,
        body: Container(
          color: Color.fromRGBO(244, 241, 235, 1),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              HeaderView(_scaffoldKey),
              Align(
                alignment: Alignment(1, 1),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.55,
                  child: flower,
                ),
              ),
              Align(
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isPressed ? isPressed = false : isPressed = true;
                    });
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.08,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                         BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 3,
                            offset: Offset(0, 2))
                      ],
                    ),
                    margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1,
                    ),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          child: Container(
                            margin: EdgeInsets.all(
                                MediaQuery.of(context).size.width * 0.04),
                            child: Text(
                              txt,
                              style: TextStyle(
                                  color: choice > 0
                                      ? Colors.black
                                      : Colors.black45
                                  ,fontFamily: "Montserrat",fontSize: 16),
                            ),
                          ),
                          alignment: Alignment(-1, 0),
                        ),
                        Align(
                          child: Container(
                            margin: EdgeInsets.all(
                                MediaQuery.of(context).size.width * 0.04),
                            child: isPressed ? iWG1 : iWG,
                          ),
                          alignment: Alignment(1, 0),
                        )
                      ],
                    ),
                  ),
                ),
                alignment: Alignment(0, -0.6),
              ),
              wg,
              Align(
                alignment: Alignment(0, 0.8),
                child: Container(
            alignment: Alignment.center,
            child: Container(
              child: InkWell(
                onTap: () async {
                  
                },
                borderRadius: BorderRadius.circular(6),
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    "AXTAR",
                    style: TextStyle(
                      fontFamily: "Montserrat-Bold",
                      color: Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: choice>0?StaticValue.mainColor:Color.fromRGBO(112, 112, 112, 1),
              borderRadius: BorderRadius.circular(6),
            ),
            height: MediaQuery.of(context).size.height * 0.08,
            margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1,
                    ),
          ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

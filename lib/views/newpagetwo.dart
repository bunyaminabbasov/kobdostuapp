import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';

class PageTwo extends StatefulWidget {
  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();
  TextStyle idleStyle = TextStyle(
    color: Colors.black54,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    
  );
  TextStyle goldStyle = TextStyle(
    color: StaticValue.mainColor,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    decorationThickness: 2.0,
  );
  int choiceState = 1;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        drawer: KobDrawer(_scaffoldKey,context),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Container(
                color: Color.fromRGBO(237, 237, 237, 1),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.23,
              ),
              HeaderView(_scaffoldKey),
              Container(
                height: MediaQuery.of(context).size.height * 0.12,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.11,
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                choiceState = 1;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 1
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Text(
                                "KOB\nDOSTU",
                                style: choiceState == 1 ? goldStyle : idleStyle,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                choiceState = 2;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 2
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Text(
                                "CAĞRI\nMƏRKƏZİ",
                                style: choiceState == 2 ? goldStyle : idleStyle,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                choiceState = 3;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 3
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Text(
                                "eKOB\nPORTAL",
                                style: choiceState == 3 ? goldStyle : idleStyle,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                choiceState = 4;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 4
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Text(
                                "HAMISI",
                                style: choiceState == 4 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                        ],
                      )
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

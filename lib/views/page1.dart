import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';

class MyPageOne extends StatefulWidget {
  @override
  _MyPageOneState createState() => _MyPageOneState();
}

class _MyPageOneState extends State<MyPageOne> {
  bool a = true;
  final myController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
   
    Widget wg1 = Container(
      margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.15,
          left: MediaQuery.of(context).size.width * 0.1),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(-1, -0.55),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.3,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Color.fromRGBO(207, 207, 207, 1),
                    blurRadius: 2.0,
                    spreadRadius: 1,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment(-1, 0.45),
            child: Container(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.1),
              alignment: Alignment(-0.7, -0.7),
              child: TextFormField(
                controller: myController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Qeyd...",
                ),
              ),
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.2,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Color.fromRGBO(207, 207, 207, 1),
                    blurRadius: 5.0,
                    spreadRadius: 1,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment(-1, 0.88),
            child: InkWell(
              onTap: () {
                setState(() {
                 a = false; 
                });
              },
              child: Container(
                alignment: Alignment(0, 0),
                child: Text(
                  "GÖNDƏR",
                  style: TextStyle(
                    fontFamily: "Montserrat-Bold",
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.08,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: myController.text == ""
                      ? Color.fromRGBO(181, 181, 181, 1)
                      : StaticValue.mainColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );

    Widget wg2 = Container(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(-0.6, -0.3),
            child: Text(
              "Vergilər Departamenti",
              style: TextStyle(
                fontFamily: "Monserrat-Medium",
                fontWeight: FontWeight.w500,
                fontSize: 19,
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.3, -0.3),
            child: Image.asset("assets/processIcon.png"),
          ),
          Align(
            alignment: Alignment(0.8, -0.3),
            child: Container(
              alignment: Alignment(0, 0),
              child: Text(
                "ƏTRAFLI",
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.height * 0.02,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
              height: MediaQuery.of(context).size.height * 0.04,
              width: MediaQuery.of(context).size.width * 0.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: StaticValue.mainColor,
                    width: 2,
                  )),
            ),
          ),
          Align(
            alignment: Alignment(-0.72, 0),
            child: Text(
              "FHN",
              style: TextStyle(
                fontFamily: "Monserrat-Medium",
                fontWeight: FontWeight.w500,
                fontSize: 19,
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.3, 0),
            child: Image.asset("assets/expireIcon.png"),
          ),
          Align(
            alignment: Alignment(0.8, 0),
            child: Container(
              alignment: Alignment(0, 0),
              child: Text(
                "ƏTRAFLI",
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.height * 0.02,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
              height: MediaQuery.of(context).size.height * 0.04,
              width: MediaQuery.of(context).size.width * 0.2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: StaticValue.mainColor,
                    width: 2,
                  )),
            ),
          ),
          Align(
            alignment: Alignment(-0.6, 0.3),
            child: Text(
              "Adliyyə Nazirliyi",
              style: TextStyle(
                fontFamily: "Monserrat-Medium",
                fontWeight: FontWeight.w500,
                fontSize: 19,
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.3, 0.3),
            child: Image.asset("assets/finalizedIcon.png"),
          ),
          Align(
            alignment: Alignment(0.8, 0.3),
            child: Container(
              alignment: Alignment(0, 0),
              child: Text(
                "ƏTRAFLI",
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.height * 0.02,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
              height: MediaQuery.of(context).size.height * 0.04,
              width: MediaQuery.of(context).size.width * 0.2,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(
                  color: StaticValue.mainColor,
                  width: 2,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment(0, 0.9),
            child: InkWell(
              onTap: () {
                setState(() {
                  a = true;
                });
              },
              child: Container(
                alignment: Alignment(0, 0),
                child: Text(
                  "GERİ DÖN",
                  style: TextStyle(
                    fontFamily: "Montserrat-Bold",
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.08,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: StaticValue.mainColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset : false,
        key: _scaffoldKey,
        drawer: KobDrawer(_scaffoldKey,context),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              HeaderView(_scaffoldKey),
              Container(
                child: a ? wg1 : wg2,
              ),
              Align(
                alignment: Alignment(-0.8, -0.65),
                child: Image.asset("assets/logos/userLogo.png"),
              ),
              Align(
                alignment: Alignment(-0.6, -0.65),
                child: Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.14,
                      top: MediaQuery.of(context).size.height * 0.017),
                  child: Text(
                    "MƏMMƏDOV ƏLİ",
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: "Montserrat-Bold",
                      color: StaticValue.mainColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

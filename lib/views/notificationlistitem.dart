import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kobdostu/models/notification.dart' as appNotification;
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/viewmodels/shifahiviewmodel.dart';
import 'package:kobdostu/viewmodels/shikayetviewmodel.dart';
import 'package:kobdostu/viewmodels/sualviewmodel.dart';
import 'package:kobdostu/views/apply.dart';
import 'package:http/http.dart' as http;
import 'package:kobdostu/views/question.dart';

class NotificationListItem extends StatelessWidget {
  final appNotification.Notification notification;
  var formatter = DateFormat('dd MMM yyyy H:m');
  NotificationListItem(this.notification);
  Future<KOBStatus> seen() async {
    print(StaticValue.accessToken);
    print(notification.id);
    try {
      String url =
          StaticValue.rootEndpoint + "/api/profileapi/SeenNotification";
      var response;
      response = await http.post(
        url,
        body: json.encode({
          'id': notification.id,
        }),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        print(response.statusCode);
        if (response.statusCode == HttpStatus.ok) {
          return KOBStatus.Okay;
        }
      } else {
        return KOBStatus.Timeout;
      }
    } catch (e) {

    }

    return KOBStatus.PoorInternet;
  }

  Future<ShifahiViewModel> getShifahi() async {
    try {
      String url = StaticValue.rootEndpoint +
          "/api/mobile/get/shifahi/${notification.dataId}";
      var response;
      response = await http
          .post(
            url,
          )
          .timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return ShifahiViewModel.json(json.decode(response.body));
        }
      }
    } catch (e) {}

    return null;
  }
    Future<SualViewModel> getSual() async {
    try {
      String url = StaticValue.rootEndpoint +
          "/api/mobile/get/sual/${notification.dataId}";
      var response;
      response = await http
          .post(
            url,
          )
          .timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return SualViewModel.json(json.decode(response.body));
        }
      }
    } catch (e) {}

    return null;
  }
    Future<ShikayetViewModel> getShikayet() async {
    try {
      String url = StaticValue.rootEndpoint +
          "/api/mobile/get/shikayet/${notification.dataId}";
      var response;
      response = await http
          .post(
            url,
          )
          .timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return ShikayetViewModel.json(json.decode(response.body));
        }
      }
    } catch (e) {}

    return null;
  }

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.height * 0.15);
    return Material(
      child: InkWell(
        onTap: () async {
          print(notification.notificationType);
          print(notification.notificationType);
          print(notification.requestType);
          switch (notification.notificationType) {
            case 1:
              switch (notification.requestType) {
                case 1:
                  var seenResult = await seen();
                  print(seenResult);
                  if (seenResult == KOBStatus.Okay) {
                    StaticValue.notifyBox.state.countDecrement();
                    var shifahiResult = await getShifahi();
                    if (shifahiResult != null) {
                      print(shifahiResult.toJSON());
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShifahiPage(
                                  shifahiModel: shifahiResult,
                                )),
                      );
                    }
                  } else {
                    StaticValue.alertDialog(context,
                        title: "Kob Dostu", description: "Xəta baş verdi.");
                  }

                  break;
                  case 2:
                  var seenResult = await seen();
                  print(seenResult);
                  if (seenResult == KOBStatus.Okay) {
                    StaticValue.notifyBox.state.countDecrement();
                    var sualResult = await getSual();
                   print(sualResult);
                    if (sualResult != null) {
                      print(sualResult.toJSON());
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => QuestionPage(
                                  sualViewModel: sualResult,
                                )),
                      );
                    }
                  } else {
                    StaticValue.alertDialog(context,
                        title: "Kob Dostu", description: "Xəta baş verdi.");
                  }

                  break;
                  case 3:
                  var seenResult = await seen();
                  print(seenResult);
                  if (seenResult == KOBStatus.Okay) {
                    StaticValue.notifyBox.state.countDecrement();
                    var shifahiResult = await getShifahi();
                    if (shifahiResult != null) {
                      print(shifahiResult.toJSON());
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShifahiPage(
                                  shifahiModel: shifahiResult,
                                )),
                      );
                    }
                  } else {
                    StaticValue.alertDialog(context,
                        title: "Kob Dostu", description: "Xəta baş verdi.");
                  }

                  break;
                default:
              }
              break;
          }
        },
        child: Container(
          height: MediaQuery.of(context).size.height * 0.15 < 102.51428571428572
              ? 102.51428571428572
              : MediaQuery.of(context).size.height * 0.15,
          width: MediaQuery.of(context).size.width,
          color: notification.isSeen
              ? Colors.white
              : StaticValue.mainColor.withOpacity(0.7),
          child: Container(
            margin: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(2),
                  child: Text(
                    notification.title,
                    style: TextStyle(
                      color: notification.isSeen
                          ? Colors.black.withOpacity(0.7)
                          : Colors.white.withOpacity(0.9),
                      fontSize: 18,
                      fontFamily: "Montserrat-Bold",
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(2),
                  child: Text(
                    notification.body,
                    style: TextStyle(
                      color: notification.isSeen
                          ? Colors.black.withOpacity(0.6)
                          : Colors.white.withOpacity(0.8),
                      fontSize: 15,
                      fontFamily: "Montserrat-Bold",
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(2),
                  child: Text(
                    formatter.format(notification.sendedAt),
                    style: TextStyle(
                      color: notification.isSeen
                          ? Colors.black.withOpacity(0.5)
                          : Colors.white.withOpacity(0.6),
                      fontSize: 14,
                      fontFamily: "Montserrat-Italic",
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:speedometer/handpainter.dart';
import 'package:speedometer/linepainter.dart';
import 'package:kobdostu/views/spdtxt.dart';
import 'package:rxdart/rxdart.dart';

class CustomSpeedOMeter extends StatefulWidget {
  int start;
  int end;
  double highlightStart;
  double highlightEnd;
  ThemeData themeData;

  PublishSubject<double> eventObservable;
  CustomSpeedOMeter(
      {this.start,
      this.end,
      this.highlightStart,
      this.highlightEnd,
      this.themeData,
      this.eventObservable}) {
    print(this.highlightEnd);
  }

  @override
  _CustomSpeedOMeterState createState() =>  _CustomSpeedOMeterState(
      this.start,
      this.end,
      this.highlightStart,
      this.highlightEnd,
      this.eventObservable);
}

class _CustomSpeedOMeterState extends State<CustomSpeedOMeter>
    with TickerProviderStateMixin {
  int start;
  int end;
  double highlightStart;
  double highlightEnd;
  PublishSubject<double> eventObservable;

  double val = 0.0;
  double Val;
  AnimationController percentageAnimationController;

  _CustomSpeedOMeterState(int start, int end, double highlightStart,
      double highlightEnd, PublishSubject<double> eventObservable) {
    this.start = start;
    this.end = end;
    this.highlightStart = highlightStart;
    this.highlightEnd = highlightEnd;
    this.eventObservable = eventObservable;

    percentageAnimationController =  AnimationController(
        vsync: this, duration:  Duration(milliseconds: 1000))
      ..addListener(() {
        setState(() {
          val = lerpDouble(val, Val, percentageAnimationController.value);
        });
      });
    this.eventObservable.listen((value) => reloadData(value));
  }

  reloadData(double value) {
    print(value);
    Val = value;
    percentageAnimationController.forward(from: 0.0);
  }

  @override
  Widget build(BuildContext context) {
    return  Center(
      child:  LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return  Container(
          height: constraints.maxWidth,
          width: constraints.maxWidth,
          child:  Stack(fit: StackFit.expand, children: <Widget>[
             Container(
              child:  CustomPaint(
                  foregroundPainter:  LinePainter(
                      lineColor: this.widget.themeData.highlightColor,
                      completeColor: this.widget.themeData.primaryColor,
                      startValue: this.start,
                      endValue: this.end,
                      startPercent: this.widget.highlightStart,
                      endPercent: this.widget.highlightEnd,
                      width: 40.0)),
            ),
             CustomPaint(
                painter:  SpeedTextPainter(
                    start: this.start, end: this.end, value: this.val)),
             Center(
                //   aspectRatio: 1.0,
                child:  Container(
                    height: constraints.maxWidth,
                    width: double.infinity,
                    padding: const EdgeInsets.all(20.0),
                    child:  Stack(fit: StackFit.expand, children: <Widget>[
                       CustomPaint(
                        painter:  HandPainter(
                            value:
                                val > end ? double.parse(end.toString()) : val,
                            start: this.start,
                            end: this.end,
                            color: this.widget.themeData.accentColor),
                      ),
                    ]))),
             Center(
              child:  Container(
                width: 30.0,
                height: 30.0,
                decoration:  BoxDecoration(
                  shape: BoxShape.circle,
                  color: this.widget.themeData.backgroundColor,
                ),
              ),
            ),
          ]),
        );
      }),
    );
  }
}

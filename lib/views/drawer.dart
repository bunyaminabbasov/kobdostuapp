import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/apply.dart';
// import 'package:kobdostu/views/complain.dart';
import 'package:kobdostu/views/customalert.dart' as customalert;
import 'package:kobdostu/views/login.dart';
import 'package:kobdostu/views/newpageone.dart';
import 'package:kobdostu/views/profile.dart';
import 'package:kobdostu/views/question.dart';
import 'package:kobdostu/views/search.dart';
import 'package:http/http.dart' as http;
import 'package:kobdostu/static/sharedpref.dart' as shared;
import 'package:rflutter_alert/rflutter_alert.dart';

import 'complain.dart';

class KobDrawer extends StatefulWidget {
  KobDrawer(this._scaffoldKey, this._mainContext);
  final GlobalKey<ScaffoldState> _scaffoldKey;
  final BuildContext _mainContext;
  @override
  _KobDrawerState createState() => _KobDrawerState();
}

class _KobDrawerState extends State<KobDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        margin: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.05,
          right: MediaQuery.of(context).size.width * 0.05,
          top: MediaQuery.of(context).size.height * 0.1,
        ),
        child: ListView(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
               
                  Navigator.of(context).pushNamed('/PageOne');
                 
               
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "MÜRACİƏT",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color:Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  Navigator.pop(context);
                 
                    Navigator.of(context).pushNamed('/applyPage');
                    
                  
                });
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ŞİFAHİ VƏRƏQƏSİ",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color:Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuestionPage()),
                  );
                 
                
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "SUAL VƏRƏQƏSİ",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color: Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
               
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ComplainPage()),
                  );
                 
                
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ŞİKAYƏT",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color:Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color:  Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                });
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "DİGƏR",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color: Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                
                });
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "STATİSTİKA",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color:Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                 
                });
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "LAHİYƏLƏR",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color: Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
               
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => ProfilePage()),
                  );
                 
               
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ŞƏXSİ HESABIM",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color: Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
               
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SearchPage()),
                  );
                  
                
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "AXTAR",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color: Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () async {
                // Navigator.of(context).pop();
                int value = 0;
                await customalert.Alert(
                  context: context,
                  type: AlertType.none,
                  title: "Kob Dostu",
                  desc: "Çıxış etməyə əminsiniz?",
                  buttons: [
                    DialogButton(
                      height: MediaQuery.of(context).size.height*0.055,
                      radius: BorderRadius.all(
                        Radius.circular(6),
                      ),
                      child: Text(
                        "Bəli",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontFamily: "Montserrat",
                        ),
                      ),
                      onPressed: () async {
                        Navigator.of(context).pop();
                        StaticValue.amazingLoading(context);
                        var result = await logOut();
                        Navigator.of(context).pop();
                        if (result == KOBStatus.Okay) {
                          
                          widget._scaffoldKey.currentState.openEndDrawer();
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (widget) => LoginPage()),
                          );
                        } else {
                          await StaticValue.alertDialog(context,
                              title: "Kob Dostu",
                              description: "Xəta baş verdi.");
                        }
                      },
                      color: Color.fromRGBO(112, 112, 112, 1.0),
                    ),
                    DialogButton(
                      height: MediaQuery.of(context).size.height*0.055,
                      radius: BorderRadius.all(
                        Radius.circular(6),
                      ),
                      child: Text(
                        "Xeyr",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontFamily: "Montserrat",
                        ),
                      ),
                      onPressed: () => {Navigator.pop(context)},
                      color: StaticValue.mainColor,
                    )
                  ],
                  style: AlertStyle(
                    overlayColor: Color.fromRGBO(0, 0, 0, 0.01),
                    alertBorder: Border.all(
                      width: 0.1,
                    ),
                    descStyle: TextStyle(
                      fontSize: 16,
                      fontFamily: "Montserrat",
                    ),
                    titleStyle: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      fontFamily: "Montserrat-Bold",
                    ),
                  ),
                ).show();
                if (value != 0) {
                  await StaticValue.amazingLoading(context);
                  var result = await logOut();
                  Navigator.of(context).pop();
                  if (result == KOBStatus.Okay) {
                    
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  } else {
                    StaticValue.alertDialog(widget._mainContext,
                        title: "Kob Dostu", description: "Xəta baş verdi.");
                  }
                }
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Çıxış",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          color:Colors.black,
                          fontSize: 18,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    color:Colors.black,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<KOBStatus> logOut() async {
    try {
      String url = StaticValue.rootEndpoint + "/api/accountapi/signout";
      var response;
      response = await http.post(
        url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken,
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          await StaticValue.nuler();
          return KOBStatus.Okay;
        } else if (response.statusCode == HttpStatus.badRequest) {
          return KOBStatus.Sobad;
        } else if (response.statusCode == HttpStatus.internalServerError) {
          return KOBStatus.InternalError;
        }
      } else {
        return KOBStatus.Timeout;
      }
    } catch (e) {}

    return KOBStatus.PoorInternet;
  }
}

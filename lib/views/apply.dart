import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kobdostu/behavior/behaviors.dart';
import 'package:kobdostu/behavior/ensurevisible.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/viewmodels/shifahiviewmodel.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';
import 'package:http/http.dart' as http;
import 'package:kobdostu/views/profile.dart';

class ShifahiPage extends StatefulWidget {
  ShifahiViewModel shifahiModel;
  bool freeze;
  ShifahiPage({this.shifahiModel,this.freeze=false});
  @override
  _ShifahiPageState createState() => _ShifahiPageState();
}

class _ShifahiPageState extends State<ShifahiPage> {
  final Widget drawerbutton = SvgPicture.asset(
    "assets/icons/drawerbutton.svg",
    semanticsLabel: 'drawer icon',
  );
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey _containerKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<FocusNode> focusNodeList = List<FocusNode>();
  List<TextEditingController> controllerList = List<TextEditingController>();
  Map<int, bool> validator = Map<int, bool>();
  HeaderView _header;
  bool isIcraci = false;
  bool isMesul = false;
  var _gonderColor = Color.fromRGBO(112, 112, 112, 1);
  DateTime selectedDate = DateTime.now();
  DateTime selectedDate2 = DateTime.now();

  String acceptDate;
  String acceptDate2 = "Razılaşdırılmış qəbul vaxtı";

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate2,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate2 = picked;
      });
  }

  bool checkValidation() {
    int count = 0;
    for (var i = 0; i < 19; i++) {
      if (validator[i]) {
        count++;
      }
    }
    return count == 19;
  }

  @override
  void initState() {
    for (var i = 0; i < 17; i++) {
      controllerList.add(TextEditingController());
      validator[i] = false;
      focusNodeList.add(FocusNode());
    }
    validator[17] = false;
    validator[18] = false;
    if (widget.shifahiModel != null) {
      var map = widget.shifahiModel.map();
      for (var i = 0, j = 0; i < 18; i++) {
        if (i == 1) {
          acceptDate = map[i];
        } else if (i == 13) {
          acceptDate2 = map[i];
          j++;
        } else {
          controllerList[j].text = map[i];
          j++;
        }
      }
      for (var i = 0; i < 17; i++) {
        validator[i] = true;
      }
      validator[17] = false;
      validator[18] = false;
      selectedDate = DateTime.parse(acceptDate);
      acceptDate = formatter.format(selectedDate);
      selectedDate2 = DateTime.parse(acceptDate2);
      acceptDate2 = formatter.format(selectedDate2);
    }

    super.initState();
  }

  var formatter = DateFormat('yyyy.MM.dd');

  @override
  Widget build(BuildContext context) {
    _header = HeaderView(_scaffoldKey);
    var _height = (MediaQuery.of(context).padding.top +
            MediaQuery.of(context).size.height * 0.11) /
        (MediaQuery.of(context).size.height);
    acceptDate = formatter.format(selectedDate);
    return Scaffold(
      key: _scaffoldKey,
      drawer: KobDrawer(_scaffoldKey, context),
      body: Stack(
        children: <Widget>[
          _header,
          Container(
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * _height + 2),
            height: (MediaQuery.of(context).size.height -
                    MediaQuery.of(context).size.height * _height) *
                0.85,
            width: MediaQuery.of(context).size.width,
            child: ScrollConfiguration(
              behavior: NoEffectScrollBehavior(),
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * 0.09),
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text(
                      "VƏTƏNDAŞLARIN ŞİFAHİ MÜRACİƏTLƏRİNİN QEYDİYYAT-NƏZARƏT VƏRƏQƏSİ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: "Montserrat-Bold",
                        color: Colors.black,
                        fontSize: 14,
                        // decoration: TextDecoration.underline,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.02),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.1),
                                  blurRadius: 3,
                                  offset: Offset(0, 2))
                            ],
                          ),
                          child: Material(
                            borderRadius: BorderRadius.circular(6),
                            color: Colors.white,
                            child: EnsureVisibleWhenFocused(
                              focusNode: focusNodeList[0],
                              child: TextFormField(
                                enabled: !widget.freeze,
                                onChanged: (txt) {
                                  if(!widget.freeze){
                                  if (txt.length > 0) {
                                    validator[0] = true;
                                    _gonderColor = checkValidation() == true
                                        ? StaticValue.mainColor
                                        : Color.fromRGBO(112, 112, 112, 1);
                                  } else {
                                    validator[0] = false;
                                    _gonderColor =
                                        Color.fromRGBO(112, 112, 112, 1);
                                  }
                                }},
                                controller: controllerList[0],
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                ),
                                decoration: InputDecoration(
                                  hintText: "Daxilolma nömrəsi",
                                  hintStyle: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black54,
                                    fontFamily: "Montserrat",
                                  ),
                                  fillColor: Colors.white,
                                  prefix: Icon(
                                    Icons.ac_unit,
                                    color: Colors.transparent,
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical:
                                          MediaQuery.of(context).size.height *
                                              0.02),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black.withOpacity(0.1),
                                    blurRadius: 3,
                                    offset: Offset(0, 2))
                              ],
                            ),
                            child: Material(
                              borderRadius: BorderRadius.circular(6),
                              color: Colors.white,
                              child: InkWell(
                                borderRadius: BorderRadius.circular(6),
                                onTap: () async {
                                  await _selectDate(context);
                                  setState(() {});
                                  print(acceptDate);
                                },
                                child: TextFormField(
                                
                                  enabled: false,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black54,
                                  ),
                                  decoration: InputDecoration(
                                    hintText: acceptDate,
                                    hintStyle: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      fontFamily: "Montserrat",
                                    ),
                                    fillColor: Colors.white,
                                    prefix: Icon(
                                      Icons.ac_unit,
                                      color: Colors.transparent,
                                    ),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical:
                                            MediaQuery.of(context).size.height *
                                                0.02),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20))),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[1],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[1] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[1] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[1],
                            focusNode: focusNodeList[1],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Ad, Soyad, Atasının adı",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[2],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[2] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[2] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[2],
                            focusNode: focusNodeList[2],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "İşlədiyi yer",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[3],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[3] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[3] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[3],
                            focusNode: focusNodeList[3],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Yaşadığı ünvanı",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[4],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[4] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[4] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[4],
                            focusNode: focusNodeList[4],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Digər əlaqə rekvizitləri",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[5],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[5] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[5] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[5],
                            focusNode: focusNodeList[5],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Əvvəlki müraciətlər",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[6],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[6] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[6] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[6],
                            focusNode: focusNodeList[6],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Qəbul edilməsinə əsas",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[7],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[7] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[7] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[7],
                            focusNode: focusNodeList[7],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText:
                                  "Qəbulu keçirən vəzifəli şəxs (lər)in adı,soyadı, vəzifəsi",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[8],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[8] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[8] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[8],
                            focusNode: focusNodeList[8],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Kimin qəbuluna gəlib (dəvət edilib)",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[9],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[9] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[9] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[9],
                            focusNode: focusNodeList[9],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Müraciətin mahiyyəti",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[10],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[10] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[10] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[10],
                            focusNode: focusNodeList[10],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Görülmüş tədbirlər",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[11],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[11] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[11] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[11],
                            focusNode: focusNodeList[11],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Aidiyyatı üzrə göndərildi",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[12],
                          child: InkWell(
                            onTap: () async {
                              if(!widget.freeze){
                                await _selectDate2(context);
                              setState(() {
                                acceptDate2 = formatter.format(selectedDate2);
                              });

                              if (acceptDate2 !=
                                  "Razılaşdırılmış qəbul vaxtı") {
                                validator[12] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[12] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                              }
                            },
                            child: TextFormField(
                              onChanged: (txt) {},
                              enabled: false,
                              controller: controllerList[12],
                              focusNode: focusNodeList[12],
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                hintText: acceptDate2,
                                hintStyle: TextStyle(
                                  color: acceptDate2 !=
                                          "Razılaşdırılmış qəbul vaxtı"
                                      ? Colors.black
                                      : Colors.black54,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Montserrat",
                                ),
                                fillColor: Colors.white,
                                prefix: Icon(
                                  Icons.ac_unit,
                                  color: Colors.transparent,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical:
                                        MediaQuery.of(context).size.height *
                                            0.02),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[13],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[13] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[13] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[13],
                            focusNode: focusNodeList[13],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText:
                                  "Qəbul zamanı verilmiş yazılı müraciətin rekvizitləri",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[14],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[14] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[14] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[14],
                            focusNode: focusNodeList[14],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText:
                                  "Müvafiq struktur bölmədə (və ya müvafiq şəxs tərəfindən) görülmüş tədbirlər",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[15],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[15] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[15] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[15],
                            focusNode: focusNodeList[15],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Qəbuldan sonra görülən işlər",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[16],
                          child: TextFormField(
                                enabled: !widget.freeze,
                            onChanged: (txt) {
                              if (txt.length > 0) {
                                validator[16] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[16] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            },
                            controller: controllerList[16],
                            focusNode: focusNodeList[16],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: "Müraciətə baxılmanın səbəbi",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                 widget.freeze?Container(): Container(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.1,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "İCRAÇI",
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                    fontFamily: "Montserrat"),
                              ),
                              Container(
                                child: Text(
                                  "Nəzarəti həyata keçirən vəzifəli şəxsin imzası",
                                  style: TextStyle(
                                    color: Color.fromRGBO(137, 137, 137, 1),
                                    fontWeight: FontWeight.w300,
                                    fontSize: 14,
                                    fontFamily: "Montserrat",
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Checkbox(
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          value: isIcraci,
                          onChanged: (e) {
                            setState(() {
                              isIcraci = isIcraci ? false : true;
                              if (isIcraci) {
                                validator[17] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[17] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            });
                          },
                          activeColor: StaticValue.mainColor,
                        ),
                      ],
                    ),
                  ),
                 widget.freeze?Container():  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.1,
                      right: MediaQuery.of(context).size.width * 0.08,
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "MƏSUL ŞƏXS",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 16,
                              fontFamily: "Montserrat",
                            ),
                          ),
                        ),
                        Checkbox(
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          value: isMesul,
                          onChanged: (e) {
                            setState(() {
                              isMesul = isMesul ? false : true;
                              if (isMesul) {
                                validator[18] = true;

                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } else {
                                validator[18] = false;
                                _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                              }
                            });
                          },
                          activeColor: StaticValue.mainColor,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
         widget.freeze?Container(): Container(
            alignment: Alignment.center,
            child: Container(
              child: InkWell(
                onTap: () async {
                  if (_gonderColor.red == StaticValue.mainColor.red) {
                    Map<int, String> shifahi = Map<int, String>();
                    for (var i = 0, j = 0; i < 18; i++) {
                      if (i == 1) {
                        shifahi[i] = acceptDate;
                      } else if (i == 13) {
                        shifahi[i] = acceptDate2;
                        j++;
                      } else {
                        shifahi[i] = controllerList[j].text;
                        j++;
                      }
                    }
                    var shifahiVM = ShifahiViewModel.form(shifahi);
                    print(shifahiVM.toJSON());
                    if (widget.shifahiModel == null) {
                      try {
                        String url =
                            StaticValue.rootEndpoint + "/api/receive/shifahi";
                        var response = await http.post(
                          url,
                          body: shifahiVM.toJSON(),
                          headers: {
                            HttpHeaders.contentTypeHeader: "application/json",
                            "X-M-Request-Origin": "1",
                            "X-Kob-Dostu-Token": StaticValue.accessToken
                                .replaceAll("Bearer ", "")
                          },
                        ).timeout(Duration(seconds: StaticValue.timeout));
                        if (response != null) {
                          if (response.statusCode == HttpStatus.ok) {
                            StaticValue.alertDialog(context,
                                title: "Kob Dostu",
                                description: "Əməlyat uğurla icra edildi.",
                                buttonAction: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()),
                              );
                            });
                            return;
                          }
                        }
                      } catch (e) {}
                      StaticValue.alertDialog(context,
                          title: "Kob Dostu", description: "Xəta baş verdi.");
                      return;
                    } else {
                      shifahiVM.id=widget.shifahiModel.id;
                      try {
                        String url = StaticValue.rootEndpoint +
                            "/api/receive/shifahi-update";
                        var response = await http.post(
                          url,
                          body: shifahiVM.toJSON(),
                          headers: {
                             HttpHeaders.contentTypeHeader: "application/json",
                            HttpHeaders.authorizationHeader:
                                "Bearer " + StaticValue.accessToken
                          },
                        ).timeout(Duration(seconds: StaticValue.timeout));
                        print(response.statusCode);
                        print(StaticValue.accessToken);
                        if (response != null) {
                          if (response.statusCode == HttpStatus.ok) {
                            StaticValue.alertDialog(context,
                                title: "Kob Dostu",
                                description: "Əməlyat uğurla icra edildi.",
                                buttonAction: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()),
                              );
                            });
                            return;
                          }
                        }
                      } catch (e) {print(e);}
                      StaticValue.alertDialog(context,
                          title: "Kob Dostu", description: "Xəta baş verdi.");
                      return;
                    }
                  }
                },
                borderRadius: BorderRadius.circular(6),
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    "GÖNDƏR",
                    style: TextStyle(
                      fontFamily: "Montserrat-Bold",
                      color: Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: _gonderColor,
              borderRadius: BorderRadius.circular(6),
            ),
            height: MediaQuery.of(context).size.height * 0.08,
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * _height +
                  2 +
                  (MediaQuery.of(context).size.height -
                          MediaQuery.of(context).size.height * _height) *
                      0.88,
              left: MediaQuery.of(context).size.width * 0.08,
              right: MediaQuery.of(context).size.width * 0.08,
            ),
          ),
        ],
      ),
    );
  }

  void _showDialog(String text, Function foo) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Kob Dostu"),
          content: Text(text),
          actions: <Widget>[
            FlatButton(
              child: Text("Bağla"),
              onPressed: () {
                Navigator.of(context).pop();
                foo();
              },
            ),
          ],
        );
      },
    );
  }
}

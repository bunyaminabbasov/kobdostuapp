import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kobdostu/behavior/behaviors.dart';
import 'package:kobdostu/behavior/ensurevisible.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/viewmodels/sualviewmodel.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';
import 'package:kobdostu/views/profile.dart';
import 'package:kobdostu/views/speed.dart';
import 'package:http/http.dart' as http;

class QuestionPage extends StatefulWidget {
  SualViewModel sualViewModel;
  bool freeze;
  QuestionPage({this.sualViewModel,this.freeze=false});
  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  Map<int, bool> validator = Map<int, bool>();
  List<FocusNode> focusNodeList = List<FocusNode>();
  List<TextEditingController> controllerList = List<TextEditingController>();

  var formatter = DateFormat('yyyy.MM.dd');
  var _gonderColor = Color.fromRGBO(112, 112, 112, 1);
  final Widget drawerbutton = SvgPicture.asset(
    "assets/icons/drawerbutton.svg",
    semanticsLabel: 'drawer icon',
  );
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey _containerKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime.now();
  DateTime selectedDate2 = DateTime.now();

  String acceptDate = "MÜRACİƏTİN QƏBUL EDİLDİYİ TARİX";
  String acceptDate2 = "MÜRACİƏTÇİ İLƏ TƏKRAR ƏLAQƏ TARİXİ";
  HeaderView _header;
  bool isImza = false;
  bool checkValidation() {
    int count = 0;
    for (var i = 0; i < 9; i++) {
      if (validator[i]) {
        count++;
      }
    }
    print(count);
    return count == 9;
  }

  @override
  void initState() {
    for (var i = 0; i < 9; i++) {
      controllerList.add(TextEditingController());
      validator[i] = false;
      focusNodeList.add(FocusNode());
    }
    if (widget.sualViewModel != null) {
      var map = widget.sualViewModel.map();
      for (var i = 0, j = 0; i < 9; i++) {
        if (i == 5) {
          acceptDate = map[i];
        } else if (i == 6) {
          acceptDate2 = map[i];
          
        } else {
          controllerList[j].text = map[i];
        }
          j++;
      }
      for (var i = 0; i < 9; i++) {
        validator[i] = true;
      }

      selectedDate = DateTime.parse(acceptDate);
      acceptDate = formatter.format(selectedDate);
      selectedDate2 = DateTime.parse(acceptDate2);
      acceptDate2 = formatter.format(selectedDate2);
    }
    super.initState();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate2,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate2 = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    _header = HeaderView(_scaffoldKey);
    var _height = (MediaQuery.of(context).padding.top +
            MediaQuery.of(context).size.height * 0.11) /
        (MediaQuery.of(context).size.height);
    return Scaffold(
      key: _scaffoldKey,
      drawer: KobDrawer(_scaffoldKey, context),
      body: Stack(
        children: <Widget>[
          _header,
          Container(
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * _height + 2),
            height: (MediaQuery.of(context).size.height -
                    MediaQuery.of(context).size.height * _height) *
                0.85,
            width: MediaQuery.of(context).size.width,
            child: ScrollConfiguration(
              behavior: NoEffectScrollBehavior(),
              child: ListView(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.09),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      
                      
                        
                        child: Text(
                          "MÜRACİƏTÇİNİN SUAL VƏRƏQƏSİ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: "Montserrat-Bold",
                            color: Colors.black,
                            fontSize: 14,
                            // decoration: TextDecoration.underline,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.09,
                      right: MediaQuery.of(context).size.width * 0.09,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "№",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Montserrat-Bold",
                          color: Colors.black,
                          fontSize: 14,
                          // decoration: TextDecoration.underline,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[0] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[0] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[0],
                          focusNode: focusNodeList[0],
                          maxLines: 5,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Ad, Soyad, Atasının adı",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                vertical:
                                    MediaQuery.of(context).size.height * 0.02),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[1] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[1] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[1],
                          focusNode: focusNodeList[1],
                          maxLines: 5,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Ünvan",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                vertical:
                                    MediaQuery.of(context).size.height * 0.02),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[2] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[2] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[2],
                          focusNode: focusNodeList[2],
                          maxLines: 5,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Əlaqə Telefon",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                vertical:
                                    MediaQuery.of(context).size.height * 0.02),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 3,
                            offset: Offset(0, 2))
                      ],
                    ),
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Material(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.white,
                      child: EnsureVisibleWhenFocused(
                        focusNode: focusNodeList[3],
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[3] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[3] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[3],
                          focusNode: focusNodeList[3],
                          maxLines: 7,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Sual:",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 3,
                            offset: Offset(0, 2))
                      ],
                    ),
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Material(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.white,
                      child: EnsureVisibleWhenFocused(
                        focusNode: focusNodeList[4],
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[4] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[4] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[4],
                          focusNode: focusNodeList[4],
                          maxLines: 7,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Cavab:",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: InkWell(
                          onTap: () async {
                            if(!widget.freeze){
                            await _selectDate(context);
                            setState(() {
                              acceptDate = formatter.format(selectedDate);
                            });

                            if (acceptDate !=
                                "MÜRACİƏTİN QƏBUL EDİLDİYİ TARİX") {
                              validator[5] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[5] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          }},
                          child: TextFormField(
                            onChanged: (txt) {},
                            enabled: false,
                            controller: controllerList[5],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: acceptDate,
                              hintStyle: TextStyle(
                                color: acceptDate !=
                                        "MÜRACİƏTİN QƏBUL EDİLDİYİ TARİX"
                                    ? Colors.black
                                    : Colors.black54,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: InkWell(
                          onTap: () async {
                            if(!widget.freeze){
                            await _selectDate2(context);
                            setState(() {
                              acceptDate2 = formatter.format(selectedDate2);
                            });

                            if (acceptDate2 !=
                                "MÜRACİƏTÇİ İLƏ TƏKRAR ƏLAQƏ TARİXİ") {
                              validator[6] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[6] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          }},
                          child: TextFormField(
                            onChanged: (txt) {},
                            enabled: false,
                            controller: controllerList[6],
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                            decoration: InputDecoration(
                              hintText: acceptDate2,
                              hintStyle: TextStyle(
                                color: acceptDate2 !=
                                        "MÜRACİƏTÇİ İLƏ TƏKRAR ƏLAQƏ TARİXİ"
                                    ? Colors.black
                                    : Colors.black54,
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Montserrat",
                              ),
                              fillColor: Colors.white,
                              prefix: Icon(
                                Icons.ac_unit,
                                color: Colors.transparent,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height *
                                      0.02),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08,
                        bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[7] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[7] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[7],
                          maxLines: 2,
                          focusNode: focusNodeList[5],
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Müraciəti qəbul edən əməkdaşın adi",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                vertical:
                                    MediaQuery.of(context).size.height * 0.02),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08,
                        bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                                enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[8] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[8] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          controller: controllerList[8],
                          maxLines: 2,
                          focusNode: focusNodeList[6],
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText:
                                "Sənədlərlə və vətəndaşların müraciətləri ilə iş departamentinin müdiri",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                vertical:
                                    MediaQuery.of(context).size.height * 0.02),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          widget.freeze?Container(): Container(
            alignment: Alignment.center,
            child: Container(
              child: InkWell(
                onTap: () async {
                  if (_gonderColor.red == StaticValue.mainColor.red) {
                    Map<int, String> shifahi = Map<int, String>();
                    for (var i = 0, j = 0; i < 9; i++) {
                      if (i == 5) {
                        shifahi[i] = acceptDate;
                      } else if (i == 6) {
                        shifahi[i] = acceptDate2;
                      } else {
                        shifahi[i] = controllerList[j].text;
                      }
                      j++;
                    }
                    var sualVM = SualViewModel.form(shifahi);
                    
                    if (widget.sualViewModel == null) {
                      try {
                        String url =
                            StaticValue.rootEndpoint + "/api/receive/sual";
                        var response = await http.post(
                          url,
                          body: sualVM.toJSON(),
                          headers: {
                            HttpHeaders.contentTypeHeader: "application/json",
                            "X-M-Request-Origin": "1",
                            "X-Kob-Dostu-Token": StaticValue.accessToken
                                .replaceAll("Bearer ", "")
                          },
                        ).timeout(Duration(seconds: StaticValue.timeout));
                        print(response.statusCode);
                        if (response != null) {
                          if (response.statusCode == HttpStatus.ok) {
                            StaticValue.alertDialog(context,
                                title: "Kob Dostu",
                                description: "Əməlyat uğurla icra edildi.",
                                buttonAction: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()),
                              );
                            });
                            return;
                          }
                        }
                      } catch (e) {print(e);}
                      StaticValue.alertDialog(context,
                          title: "Kob Dostu", description: "Xəta baş verdi.");
                      return;
                    } else {
                      sualVM.id = widget.sualViewModel.id;
                      try {
                        String url = StaticValue.rootEndpoint +
                            "/api/receive/sual-update";
                        var response = await http.post(
                          url,
                          body: sualVM.toJSON(),
                          headers: {
                            HttpHeaders.contentTypeHeader: "application/json",
                            HttpHeaders.authorizationHeader:
                                "Bearer " + StaticValue.accessToken
                          },
                        ).timeout(Duration(seconds: StaticValue.timeout));
                        print(response.statusCode);
                        print(StaticValue.accessToken);
                        if (response != null) {
                          if (response.statusCode == HttpStatus.ok) {
                            StaticValue.alertDialog(context,
                                title: "Kob Dostu",
                                description: "Əməlyat uğurla icra edildi.",
                                buttonAction: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()),
                              );
                            });
                            return;
                          }
                        }
                      } catch (e) {
                        print(e);
                      }
                      StaticValue.alertDialog(context,
                          title: "Kob Dostu", description: "Xəta baş verdi.");
                      return;
                    }
                  }
                },
                borderRadius: BorderRadius.circular(6),
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    "GÖNDƏR",
                    style: TextStyle(
                      fontFamily: "Montserrat-Bold",
                      color: Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: _gonderColor,
              borderRadius: BorderRadius.circular(6),
            ),
            height: MediaQuery.of(context).size.height * 0.08,
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * _height +
                  2 +
                  (MediaQuery.of(context).size.height -
                          MediaQuery.of(context).size.height * _height) *
                      0.88,
              left: MediaQuery.of(context).size.width * 0.08,
              right: MediaQuery.of(context).size.width * 0.08,
            ),
          ),
        ],
      ),
    );
  }
}

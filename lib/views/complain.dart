import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kobdostu/behavior/behaviors.dart';
import 'package:kobdostu/behavior/ensurevisible.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/viewmodels/shikayetviewmodel.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http/http.dart' as http;
import 'package:kobdostu/views/profile.dart';

class ComplainPage extends StatefulWidget {
  ShikayetViewModel sikayetModel;
  bool freeze;
  ComplainPage({this.sikayetModel,this.freeze=false});
  @override
  _ComplainPageState createState() => _ComplainPageState();
}

class _ComplainPageState extends State<ComplainPage> {
  final Widget drawerbutton = SvgPicture.asset(
    "assets/icons/drawerbutton.svg",
    semanticsLabel: 'drawer icon',
  );
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey _containerKey = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  HeaderView _header;
  bool isImza = false;
  DateTime selectedDate = DateTime.now();
  String acceptDate;
  Map<int, bool> validator = Map<int, bool>();
  List<FocusNode> focusNodeList = List<FocusNode>();
  List<TextEditingController> controllerList = List<TextEditingController>();
  var _gonderColor = Color.fromRGBO(112, 112, 112, 1);

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  bool checkValidation() {
    int count = 0;
    for (var i = 0; i < 7; i++) {
      if (validator[i]) {
        count++;
      }
    }
    return count == 7;
  }

  @override
  void initState() {
    for (var i = 0; i < 7; i++) {
      controllerList.add(TextEditingController());
      validator[i] = false;
      focusNodeList.add(FocusNode());
    }
    if (widget.sikayetModel != null) {
      var map = widget.sikayetModel.map();
      print(map);
      for (var i = 0, j = 0; i < 7; i++) {
        if (i == 4) {
          acceptDate = map[i];
        } else {
          controllerList[j].text = map[i];
        }
        j++;
      }
      for (var i = 0; i < 7; i++) {
        validator[i] = true;
      }

      selectedDate = DateTime.parse(acceptDate);
      acceptDate = formatter.format(selectedDate);
    }
    super.initState();
  }

  var formatter = DateFormat('yyyy.MM.dd');
  @override
  Widget build(BuildContext context) {
    _header = HeaderView(_scaffoldKey);
    var _height = (MediaQuery.of(context).padding.top +
            MediaQuery.of(context).size.height * 0.11) /
        (MediaQuery.of(context).size.height);
    acceptDate = formatter.format(selectedDate);
    return Scaffold(
      key: _scaffoldKey,
      drawer: KobDrawer(_scaffoldKey, context),
      body: Stack(
        children: <Widget>[
          _header,
          Container(
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * _height + 2),
            height: (MediaQuery.of(context).size.height -
                    MediaQuery.of(context).size.height * _height) *
                0.85,
            width: MediaQuery.of(context).size.width,
            child: ScrollConfiguration(
              behavior: NoEffectScrollBehavior(),
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * 0.09),
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text(
                      "MÜRACİƏTÇİNİN ŞİKAYƏT VƏRƏQƏSİ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: "Montserrat-Bold",
                        color: Colors.black,
                        fontSize: 14,
                        // decoration: TextDecoration.underline,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.09,
                      right: MediaQuery.of(context).size.width * 0.09,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "№",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Montserrat-Bold",
                          color: Colors.black,
                          fontSize: 14,
                          // decoration: TextDecoration.underline,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                          enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[0] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[0] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          focusNode: focusNodeList[0],
                          controller: controllerList[0],
                          style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Ad, Soyad, Atasının adı",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(vertical: 14),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                          enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[1] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[1] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          focusNode: focusNodeList[1],
                          controller: controllerList[1],
                          style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Ünvan",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(vertical: 14),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black.withOpacity(0.1),
                                    blurRadius: 3,
                                    offset: Offset(0, 2))
                              ],
                            ),
                            child: Material(
                              borderRadius: BorderRadius.circular(6),
                              color: Colors.white,
                              child: TextFormField(
                          enabled: !widget.freeze,
                                onChanged: (txt) {
                                  if (txt.length > 0) {
                                    validator[2] = true;
                                    _gonderColor = checkValidation() == true
                                        ? StaticValue.mainColor
                                        : Color.fromRGBO(112, 112, 112, 1);
                                  } else {
                                    validator[2] = false;
                                    _gonderColor =
                                        Color.fromRGBO(112, 112, 112, 1);
                                  }
                                },
                                focusNode: focusNodeList[2],
                                controller: controllerList[2],
                                style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.height *
                                      0.018,
                                  color: Colors.black,
                                ),
                                decoration: InputDecoration(
                                  hintText: "Əlaqə Telefon",
                                  hintStyle: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Montserrat",
                                      fontWeight: FontWeight.w400),
                                  fillColor: Colors.white,
                                  prefix: Icon(
                                    Icons.ac_unit,
                                    color: Colors.transparent,
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical:
                                          MediaQuery.of(context).size.height *
                                              0.02),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 3,
                            offset: Offset(0, 2))
                      ],
                    ),
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Material(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.white,
                      child: EnsureVisibleWhenFocused(
                        focusNode: focusNodeList[3],
                        child: TextFormField(
                          enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[3] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[3] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          focusNode: focusNodeList[3],
                          controller: controllerList[3],
                          maxLines: 7,
                          style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Müraciətin qısa məzmunu…",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: EnsureVisibleWhenFocused(
                          focusNode: focusNodeList[4],
                          child: InkWell(
                            onTap: () async {
                              if(!widget.freeze){
                              await _selectDate(context);
                              setState(() {
                                acceptDate = formatter.format(selectedDate);
                              });

                             
                                validator[4] = true;
                                _gonderColor = checkValidation() == true
                                    ? StaticValue.mainColor
                                    : Color.fromRGBO(112, 112, 112, 1);
                              } }
                            ,
                            child: TextFormField(
                              onChanged: (txt) {},
                              enabled: false,
                              controller: controllerList[4],
                              focusNode: focusNodeList[4],
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                hintText: acceptDate,
                                hintStyle: TextStyle(
                                  color: acceptDate !=
                                          "Razılaşdırılmış qəbul vaxtı"
                                      ? Colors.black
                                      : Colors.black54,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Montserrat",
                                ),
                                fillColor: Colors.white,
                                prefix: Icon(
                                  Icons.ac_unit,
                                  color: Colors.transparent,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical:
                                        MediaQuery.of(context).size.height *
                                            0.02),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                          enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[5] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[5] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          focusNode: focusNodeList[5],
                          controller: controllerList[5],
                          style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText: "Müraciəti qəbul edən əməkdaşın adi",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(vertical: 14),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.075,
                    width: MediaQuery.of(context).size.width * 0.41,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.08,
                        right: MediaQuery.of(context).size.width * 0.08,
                        bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 3,
                              offset: Offset(0, 2))
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                        child: TextFormField(
                          enabled: !widget.freeze,
                          onChanged: (txt) {
                            if (txt.length > 0) {
                              validator[6] = true;
                              _gonderColor = checkValidation() == true
                                  ? StaticValue.mainColor
                                  : Color.fromRGBO(112, 112, 112, 1);
                            } else {
                              validator[6] = false;
                              _gonderColor = Color.fromRGBO(112, 112, 112, 1);
                            }
                          },
                          maxLines: 2,
                          focusNode: focusNodeList[6],
                          controller: controllerList[6],
                          style: TextStyle(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                            hintText:
                                "Sənədlərlə və vətəndaşların müraciətləri ilə iş departamentinin müdiri",
                            hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.w400),
                            fillColor: Colors.white,
                            prefix: Icon(
                              Icons.ac_unit,
                              color: Colors.transparent,
                            ),
                            contentPadding: EdgeInsets.symmetric(vertical: 14),
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
         widget.freeze?Container(): Container(
            alignment: Alignment.center,
            child: Container(
              child: InkWell(
                onTap: () async {
                  if (_gonderColor.red == StaticValue.mainColor.red) {
                    Map<int, String> shikayet = Map<int, String>();
                    for (var i = 0, j = 0; i < 7; i++) {
                      if (i == 4) {
                        shikayet[i] = acceptDate;
                      }else {
                        shikayet[i] = controllerList[j].text;
                      }
                      j++;
                    }
                    var shikayetVM = ShikayetViewModel.form(shikayet);
                    print(shikayetVM.toJSON());
                    if (widget.sikayetModel == null) {
                      try {
                        String url =
                            StaticValue.rootEndpoint + "/api/receive/shikayet";
                        var response = await http.post(
                          url,
                          body: shikayetVM.toJSON(),
                          headers: {
                            HttpHeaders.contentTypeHeader: "application/json",
                            "X-M-Request-Origin": "1",
                            "X-Kob-Dostu-Token": StaticValue.accessToken
                                .replaceAll("Bearer ", "")
                          },
                        ).timeout(Duration(seconds: StaticValue.timeout));
                        if (response != null) {
                          if (response.statusCode == HttpStatus.ok) {
                            StaticValue.alertDialog(context,
                                title: "Kob Dostu",
                                description: "Əməlyat uğurla icra edildi.",
                                buttonAction: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()),
                              );
                            });
                            return;
                          }
                        }
                      } catch (e) {}
                      StaticValue.alertDialog(context,
                          title: "Kob Dostu", description: "Xəta baş verdi.");
                      return;
                    } else {
                      shikayetVM.id = widget.sikayetModel.id;
                      try {
                        String url = StaticValue.rootEndpoint +
                            "/api/receive/shikayet-update";
                        var response = await http.post(
                          url,
                          body: shikayetVM.toJSON(),
                          headers: {
                            HttpHeaders.contentTypeHeader: "application/json",
                            HttpHeaders.authorizationHeader:
                                "Bearer " + StaticValue.accessToken
                          },
                        ).timeout(Duration(seconds: StaticValue.timeout));
                        print(response.statusCode);
                        print(StaticValue.accessToken);
                        if (response != null) {
                          if (response.statusCode == HttpStatus.ok) {
                            StaticValue.alertDialog(context,
                                title: "Kob Dostu",
                                description: "Əməlyat uğurla icra edildi.",
                                buttonAction: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()),
                              );
                            });
                            return;
                          }
                        }
                      } catch (e) {
                        print(e);
                      }
                      StaticValue.alertDialog(context,
                          title: "Kob Dostu", description: "Xəta baş verdi.");
                      return;
                    }
                  }
                },
                borderRadius: BorderRadius.circular(6),
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    "GÖNDƏR",
                    style: TextStyle(
                      fontFamily: "Montserrat-Bold",
                      color: Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: _gonderColor,
              borderRadius: BorderRadius.circular(6),
            ),
            height: MediaQuery.of(context).size.height * 0.08,
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * _height +
                  2 +
                  (MediaQuery.of(context).size.height -
                          MediaQuery.of(context).size.height * _height) *
                      0.88,
              left: MediaQuery.of(context).size.width * 0.08,
              right: MediaQuery.of(context).size.width * 0.08,
            ),
          ),
        ],
      ),
    );
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kobdostu/models/muraciet.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/viewmodels/shifahiviewmodel.dart';
import 'package:http/http.dart' as http;
import 'package:kobdostu/viewmodels/shikayetviewmodel.dart';
import 'package:kobdostu/viewmodels/sualviewmodel.dart';
import 'package:kobdostu/views/complain.dart';
import 'package:kobdostu/views/question.dart';

import 'apply.dart';

class MuracietListItem extends StatelessWidget {
  final Muraciet muraciet;
  MuracietListItem(this.muraciet);
  final Widget expireIcon = SvgPicture.asset(
    "assets/icons/icon_expire.svg",
    semanticsLabel: 'ss',
  );
  final Widget finalizedIcon = SvgPicture.asset(
    "assets/icons/icon_finalized.svg",
    semanticsLabel: 'sss',
  );
  final Widget processIcon = SvgPicture.asset(
    "assets/icons/icon_process.svg",
    semanticsLabel: 'ssss',
  );
  String requestText;
  Widget icon;
    Future<ShifahiViewModel> getShifahi() async {
    try {
      String url = StaticValue.rootEndpoint +
          "/api/mobile/get/shifahi/${muraciet.requestId}";
      
      var response = await http
          .post(
            url,
          )
          .timeout(Duration(seconds: StaticValue.timeout));
          print(response.statusCode);
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return ShifahiViewModel.json(json.decode(response.body));
        }
      }
    } catch (e) {}

    return null;
  }
    Future<SualViewModel> getSual() async {
    try {
      String url = StaticValue.rootEndpoint +
          "/api/mobile/get/sual/${muraciet.requestId}";
      var response;
      response = await http
          .post(
            url,
          )
          .timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return SualViewModel.json(json.decode(response.body));
        }
      }
    } catch (e) {}

    return null;
  }
    Future<ShikayetViewModel> getShikayet() async {
    try {
      String url = StaticValue.rootEndpoint +
          "/api/mobile/get/shikayet/${muraciet.requestId}";
      
       var response = await http
          .post(
            url,
          )
          .timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          print(json.decode(response.body));
          return ShikayetViewModel.json(json.decode(response.body));
        }
      }
    } catch (e) {}

    return null;
  }
  @override
  Widget build(BuildContext context) {
    if (muraciet.requestType == 1) {
      requestText = "Şifahi";
    } else if (muraciet.requestType == 2) {
      requestText = "Sual";
    } else if (muraciet.requestType == 3) {
      requestText = "Şikayət";
    }
    if (muraciet.muracietStatus == 1) {
      icon = processIcon;
    } else if (muraciet.muracietStatus == 0) {
      icon = finalizedIcon;
    }

    return InkWell(
      splashColor: Colors.transparent,
      focusColor: Colors.transparent,
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      onTap: ()async{
        print(muraciet.requestType.toString());
      switch (muraciet.requestType) {
        case 1:

          var res=await getShifahi();
          if(res!=null){
            Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShifahiPage(
                                  shifahiModel: res,freeze: true,
                                )),
                      );
          }
          break;
          case 2:
          var res=await getSual();
          if(res!=null){
            Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => QuestionPage(
                                  sualViewModel: res,freeze: true,
                                )),
                      );
          }
          break;
          case 3:
          var res = await getShikayet();
          if(res!=null){
            Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ComplainPage(
                                  sikayetModel: res,freeze: true,
                                )),
                      );
          }
          break;
        default:
      }
      },
      child: Container(
        height: MediaQuery.of(context).size.height * 0.15 < 102.51428571428572
            ? 102.51428571428572
            : MediaQuery.of(context).size.height * 0.15,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      muraciet.muracietchi,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                      requestText,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Divider(
                    thickness: 1.3,
                  )
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * 0.08,
              child: icon,
            )
          ],
        ),
      ),
    );
  }
}

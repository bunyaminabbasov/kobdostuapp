import 'package:flutter/material.dart';
//import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kobdostu/static/static.dart';

import 'notificationbox.dart';
import 'notificationpage.dart';

class HeaderView extends StatelessWidget {
  HeaderView(this._scaffoldKey);
  final Widget drawerbutton = SvgPicture.asset(
    "assets/icons/drawerbutton.svg",
    semanticsLabel: 'drawer icon',
  );
  final GlobalKey<ScaffoldState> _scaffoldKey;
  double height;
  @override
  Widget build(BuildContext context) {
    StaticValue.notifyBox = NotificationBox(
          MediaQuery.of(context).size.width * 0.1, StaticValue.mainColor);
    height = MediaQuery.of(context).padding.top +
        MediaQuery.of(context).size.height * 0.11;
    print(height);
    return Container(
      height: height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: StaticValue.mainColor,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(35),
              bottomRight: Radius.circular(35))),
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
            child: Align(
              child: InkWell(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: Container(
                  height: 20,
                  width: 46,
                  child: drawerbutton,
                ),
              ),
              alignment: Alignment(-1, 0),
            ),
          ),
          Container(
            child: Align(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NotificationPage()),
                  );
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  alignment: Alignment.centerRight,
                  child: StaticValue.notifyBox,
                ),
              ),
              alignment: Alignment(0.9, 0),
            ),
          )
        ],
      ),
    );
  }
}

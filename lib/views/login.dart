import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/svg.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/profile.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:kobdostu/static/sharedpref.dart' as shared;
import 'package:kobdostu/views/search.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Widget logo;
  TextEditingController con1 = TextEditingController(text: "miri@vhbv.net");
  TextEditingController con2 = TextEditingController(text: "1qazZAQ!");
  List<FocusNode> focusNodeList = new List<FocusNode>();
  List<TextEditingController> controllerList = List<TextEditingController>();

  @override
  void initState() {
    for (var i = 0; i < 2; i++) {
      controllerList.add(new TextEditingController());
      focusNodeList.add(new FocusNode());
    }
   client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    super.initState();
  }

  Future<KOBStatus> passwordLogin() async {
    try {
      print(HttpHeaders.contentTypeHeader);
      String url = StaticValue.rootEndpoint + "/api/accountapi/login";
     
      var response = await http.post(
        
        url,
        body: json.encode({
          'Email': controllerList[0].text,
          'Token': controllerList[1].text,
          'Type': 'password'
        }),
        headers: {HttpHeaders.contentTypeHeader: "application/json",},
      ).timeout(Duration(seconds: StaticValue.timeout));
      print(response.body);
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          print(json.decode(response.body));
          await shared.SharedPref.saveV(
              "accesstoken", json.decode(response.body)["accessToken"]);
          await shared.SharedPref.saveV(
              "refreshtoken", json.decode(response.body)["refreshToken"]);
          StaticValue.accessToken = json.decode(response.body)["accessToken"];
          return KOBStatus.Okay;
        } else if (response.statusCode == HttpStatus.badRequest) {
          return KOBStatus.Sobad;
        } else if (response.statusCode == HttpStatus.internalServerError) {
          return KOBStatus.InternalError;
        }
      } else {
        return KOBStatus.Timeout;
      }
    } catch (e) {print(e);}

    return KOBStatus.PoorInternet;
  }

  @override
  Widget build(BuildContext context) {
    logo = new SvgPicture.asset("assets/logos/koblogo.svg",
        semanticsLabel: 'Kob Logo',
        height: MediaQuery.of(context).size.height * 0.25);

    return Scaffold(
      body: Container(
          color: StaticValue.mainColor,
          child: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                child: Image.asset(
                  "assets/logos/bothkoblogo.png",
                  color: Color.fromRGBO(255, 255, 255, 1).withOpacity(0.1),
                  fit: BoxFit.cover,
                ),
              ),
              KeyboardAvoider(
                autoScroll: true,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.1),
                        child: logo,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.05),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.16),
                              margin: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).size.height * 0.01,
                              ),
                              child: Text(
                                "E-mail address",
                                style: TextStyle(
                                    color: Colors.white54,
                                    decorationStyle: TextDecorationStyle.solid,
                                    fontSize: 14,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: "Montserrat",
                                    decoration: TextDecoration.none),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.1),
                              child: Material(
                                borderRadius: BorderRadius.circular(6),
                                color: Colors.white.withOpacity(0.3),
                                child: TextFormField(
                                  keyboardType: TextInputType.emailAddress,
                                  controller: controllerList[0],
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                  ),
                                  decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    prefix: Icon(
                                      Icons.ac_unit,
                                      color: Colors.transparent,
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical:
                                            MediaQuery.of(context).size.height *
                                                0.02),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6))),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.03),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.16),
                              margin: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).size.height * 0.01,
                              ),
                              child: Text(
                                "Password",
                                style: TextStyle(
                                    color: Colors.white54,
                                    decorationStyle: TextDecorationStyle.solid,
                                    fontSize: 14,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: "Montserrat",
                                    decoration: TextDecoration.none),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.1),
                              child: Material(
                                borderRadius: BorderRadius.circular(6),
                                color: Colors.white.withOpacity(0.3),
                                child: TextFormField(
                                  keyboardType: TextInputType.visiblePassword,
                                  obscureText: true,
                                  controller: controllerList[1],
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                  ),
                                  decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    prefix: Icon(
                                      Icons.ac_unit,
                                      color: Colors.transparent,
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical:
                                            MediaQuery.of(context).size.height *
                                                0.02),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6))),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        height: MediaQuery.of(context).size.height * 0.082,
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.06,
                          left: MediaQuery.of(context).size.width * 0.1,
                          right: MediaQuery.of(context).size.width * 0.1,
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            borderRadius: BorderRadius.circular(6),
                            onTap: () async {
                              Pattern pattern =
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                              RegExp regex = new RegExp(pattern);

                              if (regex.hasMatch(controllerList[0].text) &&
                                  controllerList[1].text.length > 0) {
                                StaticValue.amazingLoading(context);
                                var result = await passwordLogin();
                                Navigator.of(context).pop();
                                if (result == KOBStatus.Okay) {
                                  result = await getDetails();
                                  

                                   
                                  if (result == KOBStatus.Okay) {
                                    await getPhoto();
                                    // var connectiontoken =
                                    //     await _firebaseMessaging.getToken();
                                    var conres ="notoken";
                                    if(StaticValue.notifyToken!="notoken"){
                                       conres = await UpdateConnection(StaticValue.notifyToken);
                                    }
                                    if (conres != null) {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => ProfilePage(),
                                        ));
                                    } else {
                                      print(StaticValue.notifyToken);
                                      StaticValue.alertDialog(context,
                                          title: "Kob Dostu",
                                          description: "Xəta baş verdi.");
                                    }
                                  } else {
                                    StaticValue.alertDialog(context,
                                        title: "Kob Dostu",
                                        description: "Xəta baş verdi.");
                                  }
                                } else if (result == KOBStatus.Sobad) {
                                  StaticValue.alertDialog(context,
                                      title: "Kob Dostu",
                                      description:
                                          "Düzgün e-poçt və kod daxil edin.");
                                } else if (result == KOBStatus.Denied ||
                                    result == KOBStatus.Timeout ||
                                    result == KOBStatus.PoorInternet ||
                                    result == KOBStatus.InternalError) {
                                  StaticValue.alertDialog(context,
                                      title: "Kob Dostu",
                                      description: "Xəta baş verdi.");
                                }
                              } else {
                                StaticValue.alertDialog(context,
                                    title: "Kob Dostu",
                                    description:
                                        "Düzgün e-poçt və kod daxil edin.");
                              }
                            },
                            child: Container(
                              alignment: Alignment.center,
                              child: Container(
                                child: Material(
                                  color: Colors.transparent,
                                  child: Text(
                                    "Daxil Ol",
                                    style: TextStyle(
                                        fontFamily: "Montserrat-Bold",
                                        color: Colors.black54,
                                        fontSize: 18,
                                        decoration: TextDecoration.none,
                                        fontWeight: FontWeight.w800),
                                  ),
                                ),
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              height: MediaQuery.of(context).size.height * 0.07,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }

   Future<String> getPhoto() async {
    String url = StaticValue.rootEndpoint + "/api/profileapi/getphoto";

    try {
      var response = await http.get(
        url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          StaticValue.photo = json.decode(response.body)["photo"];
          await shared.SharedPref.saveV("photo", StaticValue.photo);
          return StaticValue.photo;
        }
      }
    } catch (e) {}
    StaticValue.photo = null;
    return null;
  }

   Future<KOBStatus> getDetails() async {
    String url = StaticValue.rootEndpoint + "/api/profileapi/getdetails";
    print(url);
    try {
      var response = await http.get(
        url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      print(response.body);
      if (response != null) {
        
        if (response.statusCode == HttpStatus.ok) {
          StaticValue.email = json.decode(response.body)["email"];
          StaticValue.fullname = json.decode(response.body)["fullName"];
          StaticValue.region = json.decode(response.body)["title"];
          StaticValue.address = json.decode(response.body)["address"];
          StaticValue.phone = json.decode(response.body)["phone"];
          await shared.SharedPref.saveV("email", StaticValue.email);
          await shared.SharedPref.saveV("fullname", StaticValue.fullname);
          await shared.SharedPref.saveV("region", StaticValue.region);
          await shared.SharedPref.saveV("address", StaticValue.address);
          await shared.SharedPref.saveV("phone", StaticValue.phone);
          return KOBStatus.Okay;
        }
      }
    } catch (e) {print(e+"sdsdsd");}
    return KOBStatus.Sobad;
  }

  Future<String> UpdateConnection(String token) async {
    try {
      String url =
          StaticValue.rootEndpoint + "/api/profileapi/UpdateNotifyToken";
          
      
      var response = await http.post(
        url,
        body: json.encode({'Token': token}),
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      print(response.statusCode);
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return token;
        }
      }
    } catch (e) {print("problem");}

    return null;
  }
  HttpClient client = new HttpClient();

}

import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/apply.dart';
import 'package:kobdostu/views/complain.dart';
import 'package:kobdostu/views/question.dart';

class UselessPageOne extends StatefulWidget {
  @override
  _UselessPageOneState createState() => _UselessPageOneState();
}

class _UselessPageOneState extends State<UselessPageOne> {
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: MediaQuery.of(context).size.width * 0.2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.06,
                      right: MediaQuery.of(context).size.width * 0.06,
                    ),
                    child: InkWell(
                      onTap: () {Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ShifahiPage()));},
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        alignment: Alignment.center,
                        child: Container(
                          child: Material(
                            color: Colors.transparent,
                            child: Text(
                              "ŞİFAHİ VƏRƏQƏSİ",
                              style: TextStyle(
                                  fontFamily: "Montserrat-Bold",
                                  color: Colors.black,
                                  fontSize:
                                      18,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w800),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: StaticValue.mainColor,
                          ),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        height: MediaQuery.of(context).size.height * 0.07,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.06,
                      right: MediaQuery.of(context).size.width * 0.06,
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuestionPage()));
                      },
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        alignment: Alignment.center,
                        child: Container(
                          child: Material(
                            color: Colors.transparent,
                            child: Text(
                              "SUAL VƏRƏQƏSİ",
                              style: TextStyle(
                                  fontFamily: "Montserrat-Bold",
                                  color: Colors.black,
                                  fontSize:
                                      18,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w800),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: StaticValue.mainColor,
                          ),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        height: MediaQuery.of(context).size.height * 0.07,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.06,
                      right: MediaQuery.of(context).size.width * 0.06,
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ComplainPage()));
                      },
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        alignment: Alignment.center,
                        child: Container(
                          child: Material(
                            color: Colors.transparent,
                            child: Text(
                              "ŞİKAYƏT VƏRƏQƏSİ",
                              style: TextStyle(
                                  fontFamily: "Montserrat-Bold",
                                  color: Colors.black,
                                  fontSize:
                                      18,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w800),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: StaticValue.mainColor,
                          ),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        height: MediaQuery.of(context).size.height * 0.07,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment(0.9, 0.8),
              child: Container(
                margin: EdgeInsets.all(3),
                child: FloatingActionButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  backgroundColor: StaticValue.mainColor,
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: MediaQuery.of(context).size.height * 0.04,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

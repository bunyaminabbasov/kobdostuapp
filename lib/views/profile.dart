import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
//import 'package:image_picker/image_picker.dart';
import 'package:kobdostu/behavior/ensurevisible.dart';
import 'package:kobdostu/static/static.dart';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import "package:kobdostu/static/sharedpref.dart" as shared;
import 'package:kobdostu/views/notificationbox.dart';

import 'drawer.dart';
import 'notificationpage.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final Widget drawerbutton = SvgPicture.asset(
    "assets/icons/drawerbutton.svg",
    semanticsLabel: 'drawer icon',
  );
  final Widget expire = SvgPicture.asset(
    "assets/icons/icon_expire.svg",
    semanticsLabel: 'expire icon',
  );
  final Widget finalized = SvgPicture.asset(
    "assets/icons/icon_finalized.svg",
    semanticsLabel: 'finalized icon',
  );
  final Widget process = SvgPicture.asset(
    "assets/icons/icon_process.svg",
    semanticsLabel: 'process icon',
  );
  List<FocusNode> focusNodeList = List<FocusNode>();
  List<TextEditingController> controllerList = List<TextEditingController>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isEditing = false;
  int count = 0;
  Widget image;
  String fullnameText = "User";

  @override
  void initState() {
    for (var i = 0; i < 4; i++) {
      controllerList.add(TextEditingController());
      focusNodeList.add(FocusNode());
    }
    super.initState();
  }

  Future<void> ProfilePageLoad() {
    image = StaticValue.photo == null || StaticValue.photo.trim() == ""
        ? Image.asset("assets/icons/businessman.png",
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.height * 0.2,
            fit: BoxFit.scaleDown)
        : Image.memory(
            base64Decode(StaticValue.photo),
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.height * 0.2,
            fit: BoxFit.cover,
          );
  }

  @override
  Widget build(BuildContext context) {
    if (count == 0) {
      StaticValue.notifyBox = NotificationBox(
          MediaQuery.of(context).size.width * 0.1, StaticValue.mainColor);
      fullnameText = StaticValue.fullname;
      controllerList[0].text = StaticValue.region;
      controllerList[1].text = StaticValue.address;
      controllerList[2].text = StaticValue.phone;
      controllerList[3].text = StaticValue.email;
      ProfilePageLoad();
      count++;
    }
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        drawer: KobDrawer(_scaffoldKey, context),
        body: RefreshIndicator(
          onRefresh: ProfilePageLoad,
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.4,
                  width: MediaQuery.of(context).size.width,
                  color: StaticValue.mainColor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.05,
                          right: MediaQuery.of(context).size.width * 0.05,
                        ),
                        child: Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                _scaffoldKey.currentState.openDrawer();
                              },
                              child: Container(
                                alignment: Alignment.centerLeft,
                                width: 46,
                                height: 20,
                                child: drawerbutton,
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.45 -
                                  MediaQuery.of(context).size.width *
                                      46 /
                                      MediaQuery.of(context).size.width,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NotificationPage()),
                  );
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.45,
                                alignment: Alignment.centerRight,
                                child: StaticValue.notifyBox,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.2,
                        width: MediaQuery.of(context).size.height * 0.2,
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white70,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(
                              MediaQuery.of(context).size.height * 0.15),
                          child: image,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02,
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height * 0.06,
                              width: MediaQuery.of(context).size.height * 0.06,
                            ),
                            Text(
                              fullnameText,
                              style: TextStyle(
                                  fontFamily: "Montserrat-Bold",
                                  color: Colors.white,
                                  fontSize: 18,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w700),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.06,
                              width: MediaQuery.of(context).size.height * 0.06,
                              child: isEditing
                                  ? Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                    )
                                  : InkWell(
                                      onTap: () {
                                        setState(() {
                                          isEditing = true;
                                        });
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                        ),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.05,
                                        width:
                                            MediaQuery.of(context).size.height *
                                                0.05,
                                        decoration: BoxDecoration(
                                            color: Colors.white70,
                                            shape: BoxShape.circle),
                                        child: Icon(
                                          Icons.edit,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.03,
                                          color: StaticValue.mainColor,
                                        ),
                                      ),
                                    ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.06,
                  color: Color.fromRGBO(237, 237, 237, 1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(width: 1.5, color: Colors.black38),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(
                              "0",
                              style: TextStyle(
                                  fontFamily: "Montserrat-Italic",
                                  color: Colors.black,
                                  fontSize:
                                      MediaQuery.of(context).size.height * 0.02,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: process,
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 10,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(width: 1.5, color: Colors.black38),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(
                              "0",
                              style: TextStyle(
                                  fontFamily: "Montserrat-Italic",
                                  color: Colors.black,
                                  fontSize:
                                      MediaQuery.of(context).size.height * 0.02,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: expire,
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 10,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(width: 1.5, color: Colors.black38),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(
                              "0",
                              style: TextStyle(
                                  fontFamily: "Montserrat-Italic",
                                  color: Colors.black,
                                  fontSize:
                                      MediaQuery.of(context).size.height * 0.02,
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                            child: finalized,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 10,
                            ),
                          ),
                          Container(width: 1.5, color: Colors.black38),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.35,
                          margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.02,
                            left: MediaQuery.of(context).size.width * 0.05,
                            right: MediaQuery.of(context).size.width * 0.05,
                          ),
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015,
                                  ),
                                  child: Center(
                                    child: Text(
                                      "PERSONAL İNFORMATİON",
                                      style: TextStyle(
                                          fontFamily: "Montserrat-SemiBold",
                                          color: Colors.black,
                                          fontSize: 14,
                                          decoration: TextDecoration.none,
                                          fontWeight: FontWeight.w200),
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.8,
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          "REGİON: ",
                                          style: TextStyle(
                                            fontFamily: "Montserrat",
                                            color: Colors.black,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          child: TextField(
                                            controller: controllerList[0],
                                            style: TextStyle(
                                              color: Colors.black38,
                                              fontSize: 14,
                                              fontFamily: "Montserrat",
                                            ),
                                            enabled: false,
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        vertical: 1,
                                                        horizontal: 5),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                )),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.8,
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          "ADDRESS: ",
                                          style: TextStyle(
                                            fontFamily: "Montserrat",
                                            color: Colors.black,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          child: TextField(
                                            controller: controllerList[1],
                                            style: TextStyle(
                                              color: Colors.black38,
                                              fontSize: 14,
                                              fontFamily: "Montserrat",
                                            ),
                                            enabled: false,
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        vertical: 1,
                                                        horizontal: 5),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                )),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.8,
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          "PHONE: ",
                                          style: TextStyle(
                                            fontFamily: "Montserrat",
                                            color: Colors.black,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          child: TextField(
                                            keyboardType: TextInputType.phone,
                                            controller: controllerList[2],
                                            style: TextStyle(
                                              color: isEditing
                                                  ? Colors.black
                                                  : Colors.black38,
                                              fontSize: 14,
                                              fontFamily: "Montserrat",
                                            ),
                                            enabled: isEditing,
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        vertical: 1,
                                                        horizontal: 5),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                )),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.8,
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          "EMAIL: ",
                                          style: TextStyle(
                                            fontFamily: "Montserrat",
                                            color: Colors.black,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          child: EnsureVisibleWhenFocused(
                                            focusNode: focusNodeList[3],
                                            child: TextField(
                                              focusNode: focusNodeList[3],
                                              controller: controllerList[3],
                                              style: TextStyle(
                                                color: Colors.black38,
                                                fontSize: 14,
                                                fontFamily: "Montserrat",
                                              ),
                                              enabled: false,
                                              decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          vertical: 1,
                                                          horizontal: 5),
                                                  border: OutlineInputBorder(
                                                    borderSide: BorderSide.none,
                                                  )),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        isEditing
                            ? GestureDetector(
                                onTap: () async {
                                  setState(() {
                                    isEditing = false;
                                  });
                                  if(StaticValue.phone!=controllerList[2].text){
                                    StaticValue.amazingLoading(context);
                                  try {
                                    String url = StaticValue.rootEndpoint +
                                        "/api/profileapi/update";
                                    var response;
                                    response = await http.post(
                                      url,
                                      body: json.encode(
                                          {'phone': controllerList[2].text}),
                                      headers: {
                                        HttpHeaders.contentTypeHeader:
                                            "application/json",
                                        HttpHeaders.authorizationHeader:
                                            "Bearer " + StaticValue.accessToken
                                      },
                                    ).timeout(
                                        Duration(seconds: StaticValue.timeout));
                                    Navigator.of(context).pop();
                                    if (response != null) {
                                      if (response.statusCode ==
                                          HttpStatus.ok) {
                                        await shared.SharedPref.saveV(
                                            "phone", controllerList[2].text);
                                        StaticValue.phone =
                                            controllerList[2].text;
                                        return;
                                      }
                                    }
                                  } catch (e) {
                                    Navigator.of(context).pop();
                                  }
                                  controllerList[2].text = StaticValue.phone;
                                  StaticValue.alertDialog(context,
                                      title: "Kob Dostu",
                                      description: "Xəta baş verdi.");
                                  }
                                  

                                  return;
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Container(
                                    child: Material(
                                      color: Colors.transparent,
                                      child: Text(
                                        "TƏSDİQLƏ",
                                        style: TextStyle(
                                          fontFamily: "Montserrat-Bold",
                                          color: Colors.white,
                                          fontSize: 18,
                                          decoration: TextDecoration.none,
                                          fontWeight: FontWeight.w800,
                                        ),
                                      ),
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                    color: StaticValue.mainColor,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  height:
                                      MediaQuery.of(context).size.height * 0.08,
                                  margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.02,
                                    left: MediaQuery.of(context).size.width *
                                        0.06,
                                    right: MediaQuery.of(context).size.width *
                                        0.06,
                                  ),
                                ),
                              )
                            : Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.07,
                              ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showDialog(String text) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Kob Dostu"),
          content: Text(text),
          actions: <Widget>[
            FlatButton(
              child: Text("Bağla"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

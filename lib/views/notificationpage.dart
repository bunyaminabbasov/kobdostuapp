import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/notificationlistitem.dart';
import 'package:kobdostu/models/notification.dart' as appNotification;
import 'package:http/http.dart' as http;
import 'package:kobdostu/models/notification.dart' as appNotification;

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List<NotificationListItem> notifications = List<NotificationListItem>();

  Future update() async {
    String url =
        StaticValue.rootEndpoint + "/api/profileapi/GetAllNotifications";
    try {
      var response = await http.get(
        url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        print(response.body);
        if (response.statusCode == HttpStatus.ok) {
          var _notifications = (json.decode(response.body));
          int length = _notifications["notifications"].length;
          print("length" + length.toString());
          setState(() {
           for (int i = 0; i < length; i++) {
             print(_notifications["notifications"][i]["id"]);
            notifications.add(
              NotificationListItem(
                appNotification.Notification(
                    title: _notifications["notifications"][i]["title"],
                    body: _notifications["notifications"][i]["body"],
                    sendedAt: DateTime.parse(_notifications["notifications"][i]
                            ["sendedAt"]
                        .toString()
                        .substring(0, 10)),
                    isSeen: _notifications["notifications"][i]["isSeen"],
                    notificationType: _notifications["notifications"][i]
                        ["notificationType"],
                    requestType: _notifications["notifications"][i]
                        ["requestType"],
                    dataId: _notifications["notifications"][i]["dataId"],
                    id: _notifications["notifications"][i]["id"]),
              ),
            );
          } 
          notifications=notifications.reversed.toList();
          }
          );

        
        }
      }
    } catch (e) {print(e);}
print("e");
  }

  @override
  void initState() {
    update();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: StaticValue.mainColor,
          title: Text(
            "Bildirişlər",
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Montserrat-Bold",
                fontSize: 20),
          ),
        ),
        body: notifications.length > 0
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  itemBuilder: (context, position) {
                    return notifications[position];
                  },
                  itemCount: notifications.length,
                ),
              )
            : Center(
                child: Text(
                  "Bildiriş yooxdur",
                  style: TextStyle(
                      color: Colors.black54,
                      fontFamily: "Montserrat",
                      fontSize: 14),
                ),
              ),
      ),
    );
  }
}

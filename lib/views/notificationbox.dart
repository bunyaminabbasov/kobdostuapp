import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:http/http.dart' as http;

class NotificationBox extends StatefulWidget {
  final double _size;
  final Color _color;
  final state = _NotificationBoxState();
  NotificationBox(this._size, this._color);
  @override
  _NotificationBoxState createState() => state;
}

class _NotificationBoxState extends State<NotificationBox> {
  int notificationCount = 0;
  countIncrement() {
    setState(() {
      notificationCount++;
    });
  }
  countDecrement() {
    if(notificationCount-1>0||notificationCount-1==0){
    setState(() {
      notificationCount--;
    });
    }
  }

  zero() {
    setState(() {
      notificationCount = 0;
    });
  }

 updateCount()async{

    String url = StaticValue.rootEndpoint + "/api/profileapi/GetNewNotificationsCount";
    try {
      var response = await http.get(
        url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + StaticValue.accessToken
        },
      ).timeout(Duration(seconds: StaticValue.timeout));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
         setState(() {
          notificationCount=json.decode(response.body)["count"]; 
          print("notificationCount: "+notificationCount.toString());
         });          
        }
      }
    } catch (e) {}
 }
  
int buildCount=0;
  @override
  Widget build(BuildContext context) {
    if(buildCount==0){
    if(StaticValue.accessToken!=null){
      updateCount();
    }
      buildCount++;
    }
    return Container(
      height: widget._size,
      width: widget._size,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(1, 1),
            child: Container(
              height: widget._size / 1.2,
              width: widget._size / 1.2,
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: widget._color
                          .withOpacity(notificationCount == 0 ? 0.6 : 1),
                      width: 2)),
              child: Icon(
                Icons.notifications,
                size: widget._size / 2,
                color:
                    widget._color.withOpacity(notificationCount == 0 ? 0.6 : 1),
              ),
            ),
          ),
          Align(
            alignment: Alignment(-1, -0.7),
            child: Container(
              height: widget._size / 2,
              width: widget._size / 2,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
              child: Container(
                alignment: Alignment.center,
                height: widget._size / 2,
                width: widget._size / 2,
                decoration: BoxDecoration(
                    color: widget._color
                        .withOpacity(notificationCount == 0 ? 0.6 : 1),
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 2)),
                child: Text(
                  notificationCount.toString(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: widget._size / 5.5,
                      fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/drawer.dart';
import 'package:kobdostu/views/header.dart';
import 'package:kobdostu/views/search.dart';
import 'package:kobdostu/views/uselesspageone.dart';

class RejectPage extends StatefulWidget {
  @override
  _RejectPageState createState() => _RejectPageState();
}

class _RejectPageState extends State<RejectPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey =  GlobalKey<ScaffoldState>();
  TextStyle idleStyle = TextStyle(
    color: Colors.black54,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
  );
  TextStyle goldStyle = TextStyle(
    color: StaticValue.mainColor,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    decorationThickness: 2.0,
  );
  int choiceState = 1;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        drawer: KobDrawer(_scaffoldKey,context),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Container(
                color: Color.fromRGBO(237, 237, 237, 1),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.23,
              ),
              HeaderView(_scaffoldKey),
              Container(
                height: MediaQuery.of(context).size.height * 0.12,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.11,
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              print("Hello");
                              setState(() {
                                choiceState = 1;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 1
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                "ACIQ",
                                style: choiceState == 1 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              print("World");
                              setState(() {
                                choiceState = 2;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 2
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                "BAĞLI",
                                style: choiceState == 2 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            onTap: () {
                              setState(() {
                                choiceState = 3;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: choiceState == 3
                                        ? StaticValue.mainColor
                                        : Colors.transparent,
                                    width: 2.5,
                                  ),
                                ),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                "HAMISI",
                                style: choiceState == 3 ? goldStyle : idleStyle,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment(0.9, 1),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.25,
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(5),
                        child: FloatingActionButton(
                          heroTag: "srch",
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SearchPage()),
                            );
                          },
                          backgroundColor: Color.fromRGBO(253, 251, 244, 1),
                          child: Icon(
                            Icons.search,
                            color: Colors.black,
                            size: MediaQuery.of(context).size.height * 0.04,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(5),
                        child: FloatingActionButton(
                        heroTag: "add",
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UselessPageOne()),
                          );
                        },
                        backgroundColor: StaticValue.mainColor,
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: MediaQuery.of(context).size.height * 0.04,
                        ),
                      ),),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

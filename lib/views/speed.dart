import 'package:flutter/material.dart';
import 'package:kobdostu/views/ss.dart';
import 'package:rxdart/rxdart.dart';
import 'package:speedometer/speedometer.dart';

class HelpMe extends StatefulWidget {
  @override
  _HelpMeState createState() => _HelpMeState();
}

class _HelpMeState extends State<HelpMe> {
  PublishSubject<double> a = PublishSubject<double>(
      onListen: () {
        print("Hello");
        return 5;
      },
      onCancel: () {});
  double count = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("assets/ymh.jpg"),fit: BoxFit.cover)
          ),
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment(0, 0),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: CustomSpeedOMeter(
                    start: 0,
                    end: 250,
                    eventObservable: a,
                    highlightStart: 50,
                    highlightEnd: 100,
                    themeData: ThemeData(
                        backgroundColor: Colors.orange,
                        accentColor: Colors.redAccent[800],
                        primaryColor: Colors.orange.withOpacity(0.2),
                        highlightColor: Colors.red),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, 0.6),
                child: InkWell(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  onTap: () {
                    count += 3;
                    a.add(count);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: Colors.orange,
                    ),
                    height: 50,
                    width: 150,
                    child: Text(
                      "Dgin Dgin",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

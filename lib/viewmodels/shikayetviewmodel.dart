class ShikayetViewModel {
  ShikayetViewModel.form(Map<int, String> shifahi) {
    adSoyadAta = shifahi[0];
    unvan = shifahi[1];
    telefon = shifahi[2];
    mezmun = shifahi[3];
    qebulTarixi = shifahi[4];
    qebulEdenAdSoyad = shifahi[5];
    sVMudur = shifahi[6];
  }
  String toJSON() {
    var json = "{" +
        "\"adSoyadAta\": \"$adSoyadAta\"," +
        "\"unvan\": \"$unvan\"," +
        "\"telefon\": \"$telefon\"," +
        "\"mezmun\": \"$mezmun\"," +
        "\"qebulTarixi\": \"$qebulTarixi\"," +
        "\"qebulEdenAdSoyad\": \"$qebulEdenAdSoyad\"," +
        "\"sVMudur\": \"$sVMudur\"," +
        "}";
    return json;
  }

  ShikayetViewModel.json(dynamic shifahi) {
    id = shifahi["id"];
    adSoyadAta = shifahi["adSoyadAta"];
    unvan = shifahi["unvan"];
    telefon = shifahi["telefon"];
    mezmun = shifahi["mezmun"];
    qebulTarixi = shifahi["qebulTarixi"];
    qebulEdenAdSoyad = shifahi["qebulEdenAdSoyad"];
    sVMudur 
    
    = shifahi["svMudur"];
  }
  Map<int, String> map() {
    Map<int, String> map = Map<int, String>();
    map[0] = adSoyadAta;
    map[1] = unvan;
    map[2] = telefon;
    map[3] = mezmun;
    map[4] = qebulTarixi;
    map[5] = qebulEdenAdSoyad;
    map[6] = sVMudur;
 
    return map;
  }

  int id;

  String adSoyadAta;

  String unvan;

  String telefon;

  String mezmun;

  String qebulTarixi;

  String qebulEdenAdSoyad;

  String sVMudur;
}

class SualViewModel {
  SualViewModel.form(Map<int, String> shifahi) {
    adSoyadAta = shifahi[0];
    unvan = shifahi[1];
    telefon = shifahi[2];
    muracietSual = shifahi[3];
    muracietCavab = shifahi[4];
    qebulTarixi = shifahi[5];
    tekrarElaqeVaxti = shifahi[6];
    qebulEdenAdSoyad = shifahi[7];
    sVMudur = shifahi[8];
  }
  SualViewModel.json(dynamic shifahi) {
    id = shifahi["id"];
    adSoyadAta = shifahi["adSoyadAta"];
    unvan = shifahi["unvan"];
    telefon = shifahi["telefon"];
    muracietSual = shifahi["muracietSual"];
    muracietCavab = shifahi["muracietCavab"];
    qebulTarixi = shifahi["qebulTarixi"];
    tekrarElaqeVaxti = shifahi["tekrarElaqeVaxti"];
    qebulEdenAdSoyad = shifahi["qebulEdenAdSoyad"];
    sVMudur = shifahi["svMudur"];
  }

  String toJSON() {
    var json = "{" +
        "\"adSoyadAta\": \"$adSoyadAta\"," +
        "\"unvan\": \"$unvan\"," +
        "\"telefon\": \"$telefon\"," +
        "\"muracietSual\": \"$muracietSual\"," +
        "\"muracietCavab\": \"$muracietCavab\"," +
        "\"qebulTarixi\": \"$qebulTarixi\"," +
        "\"tekrarElaqeVaxti\": \"$tekrarElaqeVaxti\"," +
        "\"qebulEdenAdSoyad\": \"$qebulEdenAdSoyad\"," +
        "\"sVMudur\": \"$sVMudur\"" +
        "}";
    return json;
  }

    Map<int, String> map() {
    Map<int, String> map = Map<int, String>();
     map[0] = adSoyadAta;
     map[1] = unvan;
     map[2] = telefon;
     map[3] = muracietSual;
     map[4] = muracietCavab;
     map[5] = qebulTarixi;
     map[6] = tekrarElaqeVaxti;
     map[7] = qebulEdenAdSoyad;
     map[8] = sVMudur;
    return map;
  }

     int id;

  String adSoyadAta;

  String unvan;

  String telefon;  

  String muracietSual;

  String muracietCavab;

  String qebulTarixi;

  String tekrarElaqeVaxti;

  String qebulEdenAdSoyad;

  String sVMudur;
}

class ShifahiViewModel {
  ShifahiViewModel.form(Map<int, String> shifahi) {
    daxilolmaNomresi = shifahi[0];
    daxilolmaTarixi = shifahi[1];
    adSoyadAta = shifahi[2];
    ishYeri = shifahi[3];
    unvan = shifahi[4];
    digerElaqeRekvizit = shifahi[5];
    evvelkiMuracietler = shifahi[6];
    qebulEdilmesineEsas = shifahi[7];
    qebuluKecirenShexslerinAdSoyadVezife = shifahi[8];
    kiminQebulunaGelib = shifahi[9];
    mahiyyeti = shifahi[10];
    gorulmushTedbirler = shifahi[11];
    aidiyyatiUzreGonderildi = shifahi[12];
    razilashdirilmishQebulVaxti = shifahi[13];
    qZVerilmishYaziliMuracietlerinRekvizitleri = shifahi[14];
    muvafiqStrukturBolmedeGorulmushTedbirler = shifahi[15];
    qebuldanSonraGorulenIshler = shifahi[16];
    muracieteBaxilmaninNeticesi = shifahi[17];
  }
  ShifahiViewModel.json(dynamic shifahi) {
    id = shifahi["id"];
    daxilolmaNomresi = shifahi["daxilolmaNomresi"];
    daxilolmaTarixi = shifahi["daxilolmaTarixi"];
    adSoyadAta = shifahi["adSoyadAta"];
    ishYeri = shifahi["ishYeri"];
    unvan = shifahi["unvan"];
    digerElaqeRekvizit = shifahi["digerElaqeRekvizit"];
    evvelkiMuracietler = shifahi["evvelkiMuracietler"];
    qebulEdilmesineEsas = shifahi["qebulEdilmesineEsas"];
    qebuluKecirenShexslerinAdSoyadVezife =
        shifahi["qebuluKecirenShexslerinAdSoyadVezife"];
    kiminQebulunaGelib = shifahi["kiminQebulunaGelib"];
    mahiyyeti = shifahi["mahiyyeti"];
    gorulmushTedbirler = shifahi["gorulmushTedbirler"];
    aidiyyatiUzreGonderildi = shifahi["aidiyyatiUzreGonderildi"];
    razilashdirilmishQebulVaxti = shifahi["razilashdirilmishQebulVaxti"];
    qZVerilmishYaziliMuracietlerinRekvizitleri =
        shifahi["qzVerilmishYaziliMuracietlerinRekvizitleri"];
    muvafiqStrukturBolmedeGorulmushTedbirler =
        shifahi["muvafiqStrukturBolmedeGorulmushTedbirler"];
    qebuldanSonraGorulenIshler = shifahi["qebuldanSonraGorulenIshler"];
    muracieteBaxilmaninNeticesi = shifahi["muracieteBaxilmaninNeticesi"];
  }
  String toJSON() {
    var json = "{" +
        "\"id\": \"$id\"," +
        "\"daxilolmaNomresi\": \"$daxilolmaNomresi\"," +
        "\"daxilolmaTarixi\": \"$daxilolmaTarixi\"," +
        "\"adSoyadAta\": \"$adSoyadAta\"," +
        "\"ishYeri\": \"$ishYeri\"," +
        "\"unvan\": \"$unvan\"," +
        "\"digerElaqeRekvizit\": \"$digerElaqeRekvizit\"," +
        "\"evvelkiMuracietler\": \"$evvelkiMuracietler\"," +
        "\"qebulEdilmesineEsas\": \"$qebulEdilmesineEsas\"," +
        "\"qebuluKecirenShexslerinAdSoyadVezife\": \"$qebuluKecirenShexslerinAdSoyadVezife\"," +
        "\"kiminQebulunaGelib\": \"$kiminQebulunaGelib\"," +
        "\"mahiyyeti\": \"$mahiyyeti\"," +
        "\"gorulmushTedbirler\": \"$gorulmushTedbirler\"," +
        "\"aidiyyatiUzreGonderildi\": \"$aidiyyatiUzreGonderildi\"," +
        "\"razilashdirilmishQebulVaxti\": \"$razilashdirilmishQebulVaxti\"," +
        "\"qzVerilmishYaziliMuracietlerinRekvizitleri\": \"$qZVerilmishYaziliMuracietlerinRekvizitleri\"," +
        "\"muvafiqStrukturBolmedeGorulmushTedbirler\": \"$muvafiqStrukturBolmedeGorulmushTedbirler\"," +
        "\"qebuldanSonraGorulenIshler\": \"$qebuldanSonraGorulenIshler\"," +
        "\"muracieteBaxilmaninNeticesi\": \"$muracieteBaxilmaninNeticesi\"" +
        "}";
    return json;
  }

  Map<int, String> map() {
    Map<int, String> map = Map<int, String>();
    map[0] = daxilolmaNomresi;
    map[1] = daxilolmaTarixi;
    map[2] = adSoyadAta;
    map[3] = ishYeri;
    map[4] = unvan;
    map[5] = digerElaqeRekvizit;
    map[6] = evvelkiMuracietler;
    map[7] = qebulEdilmesineEsas;
    map[8] = qebuluKecirenShexslerinAdSoyadVezife;
    map[9] = kiminQebulunaGelib;
    map[10] = mahiyyeti;
    map[11] = gorulmushTedbirler;
    map[12] = aidiyyatiUzreGonderildi;
    map[13] = razilashdirilmishQebulVaxti;
    map[14] = qZVerilmishYaziliMuracietlerinRekvizitleri;
    map[15] = muvafiqStrukturBolmedeGorulmushTedbirler;
    map[16] = qebuldanSonraGorulenIshler;
    map[17] = muracieteBaxilmaninNeticesi;
    return map;
  }

  int id = 0;

  String daxilolmaNomresi;

  String daxilolmaTarixi;

  String adSoyadAta;

  String ishYeri;

  String unvan;

  String digerElaqeRekvizit;

  String evvelkiMuracietler;

  String qebulEdilmesineEsas;

  String kiminQebulunaGelib;

  String mahiyyeti;

  String gorulmushTedbirler;

  String aidiyyatiUzreGonderildi;

  String razilashdirilmishQebulVaxti;

  String qZVerilmishYaziliMuracietlerinRekvizitleri;

  String muvafiqStrukturBolmedeGorulmushTedbirler;

  String qebuldanSonraGorulenIshler;

  String muracieteBaxilmaninNeticesi;

  String qebuluKecirenShexslerinAdSoyadVezife;
}

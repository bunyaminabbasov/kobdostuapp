
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kobdostu/static/sharedpref.dart';
//import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:kobdostu/static/static.dart';
import 'package:kobdostu/views/apply.dart';
import 'package:kobdostu/views/complain.dart';
import 'package:kobdostu/views/newpageone.dart';
import 'package:kobdostu/views/newpagetwo.dart';
import 'package:kobdostu/views/notificationbox.dart';
import 'package:kobdostu/views/profile.dart';
import 'package:kobdostu/views/question.dart';
import 'package:kobdostu/views/search.dart';
import 'package:kobdostu/views/splashscreen.dart';


main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
    statusBarColor: Colors.transparent,
  ));
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.paused:
        print("paused");
        await SharedPref.saveV("appstate", "p");
        break;
      case AppLifecycleState.resumed:
        print("resumed");
        await SharedPref.saveV("appstate", "r");
        break;
      case AppLifecycleState.inactive:
        print("inactive");
        break;
      case AppLifecycleState.suspending:
        print("suspended");
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/profilePage': (BuildContext context) => ProfilePage(),
        '/applyPage': (BuildContext context) => ShifahiPage(),
        '/complainPage': (BuildContext context) => ComplainPage(),
        '/questionPage': (BuildContext context) => QuestionPage(),
        '/PageOne': (BuildContext context) => PageOne(),
        '/PageTwo': (BuildContext context) => PageTwo(),
        '/searchPage': (BuildContext context) => SearchPage(),
      },
      theme: ThemeData(
        primaryColor: StaticValue.mainColor,
        accentColor: StaticValue.mainColor,
      ),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: SplashScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    wg.state.countIncrement();
  }

  var wg = NotificationBox(200, Colors.red);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
          child: Container(
        height: 200,
        width: 200,
        child: wg,
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

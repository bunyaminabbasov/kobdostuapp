import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kobdostu/views/notificationbox.dart';
import 'dart:convert';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:kobdostu/views/customalert.dart' as customAlert;

import 'package:kobdostu/static/sharedpref.dart' as shared;

class StaticValue {
  static double statusBarHeight;

  static String rootEndpoint = "http://localhost:5004";

  static String accessToken;
  static String photo;
  static String fullname;
  static String phone;
  static String address;
  static String region;
  static String refreshToken;
  static String email;
  static int timeout = 50;
  static String notifyToken="notoken";
  static Color mainColor = Color.fromRGBO(151, 126, 46, 1);
  static NotificationBox notifyBox;
  //shared

  //utils

  static nuler() async {
    accessToken = null;
    photo = null;
    fullname = null;
    phone = null;
    address = null;
    region = null;
    refreshToken = null;
    email = null;
    await shared.SharedPref.clearV();
  }

  static amazingLoading(BuildContext context) async {
    await showGeneralDialog(
      transitionDuration: Duration(milliseconds: 20),
      barrierColor: Color.fromRGBO(255, 255, 255, 0.1),
      barrierDismissible: false,
      context: context,
      pageBuilder: (context, a, b) {
        return WillPopScope(
          onWillPop: () {
            return;
          },
          child: Container(
            child: Container(
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white70),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  static Stream fcmStream;
  //httprequests

  //Refresh Login
  static Future<KOBStatus> refreshLogin(String email, String refresh) async {
    try {
      String url = rootEndpoint + "/api/accountapi/login";
      var response = await http.post(
        url,
        body:
            json.encode({'Email': email, 'Token': refresh, 'Type': 'refresh'}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 30));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          await shared.SharedPref.saveV(
              "accestoken", json.decode(response.body)["accessToken"]);
          await shared.SharedPref.saveV(
              "refreshtoken", json.decode(response.body)["refreshToken"]);
          return KOBStatus.Okay;
        } else {
          return KOBStatus.Denied;
        }
      }
    } catch (e) {}

    return KOBStatus.PoorInternet;
  }

  static notificationAlert(
    BuildContext context, {
    String message = "Hello",
    String ok = "Ok",
    String cancel = "Cancel",
    String description = "World",
    Function okAction,
  }) {
    if (okAction == null) {
      okAction = () {};
    }

    showDialog(
      context: context,
      builder: (_) => NetworkGiffyDialog(
        buttonOkColor: StaticValue.mainColor,
        image: Image.asset(
          "assets/logos/notification.png",
          fit: BoxFit.contain,
          filterQuality: FilterQuality.high,
        ),
        title: Text(message,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600)),
        description: Text(
          description,
          textAlign: TextAlign.center,
        ),
        onOkButtonPressed: okAction,
        buttonOkText: Text(
          ok,
          style: TextStyle(color: Colors.white),
        ),
        buttonCancelText: Text(
          cancel,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  static alertDialog(BuildContext context,
      {String title = "Hello",
      String description = "World",
      Function buttonAction,
      String buttonText = "Close"}) {
    if (buttonAction == null) {
      buttonAction = () {
        Navigator.of(context, rootNavigator: true).pop();
      };
    }
    customAlert.Alert(
            context: context,
            title: title,
            buttonText: buttonText,
            buttonAction: buttonAction,
            
            style: AlertStyle(
                overlayColor: Color.fromRGBO(0, 0, 0, 0.01),
                alertBorder: Border.all(
                  style: BorderStyle.none,
                ),
                titleStyle: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Montserrat-Bold",
                  color: mainColor
                )),
            desc: description)
        .show();
  }

  static Future<String> UpdateConnection(String token) async {
    if (accessToken != null) {
      if (accessToken.length > 0) {
        try {
          String url =
              StaticValue.rootEndpoint + "/api/profileapi/updateconnection";
          var response;
          response = await http.post(
            url,
            body: json.encode({'Token': token}),
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer " + StaticValue.accessToken
            },
          ).timeout(Duration(seconds: StaticValue.timeout));
          if (response != null) {
            if (response.statusCode == HttpStatus.ok) {
              return token;
            }
          }
        } catch (e) {}
      }
    }

    return null;
  }
}

enum KOBStatus {
  Okay,
  Sobad,
  Denied,
  PoorInternet,
  InternalError,
  Timeout,
  InvalidCreds
}
enum SharedStatus { Exist, NotExist }

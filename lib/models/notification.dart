import 'package:flutter/cupertino.dart' as prefix0;

class Notification {
  Notification.json(Map<String, dynamic> notify)
      : title = notify["title"],
        body = notify["body"],
        sendedAt = DateTime.parse(notify["sendedAt"].toString()),
        isSeen = notify["isSeen"],
        id = notify["isSeen"],
        notificationType = notify["notificationType"],
        requestType = notify["requestType"] {}
  Notification(
      {this.title,
      this.body,
      this.sendedAt,
      this.isSeen,
      this.id,
      this.notificationType,
      this.requestType,
      this.dataId}){print("hello");}
  String title;
  String body;
  DateTime sendedAt;
  bool isSeen;
  int id;
  int notificationType;
  int requestType;
  int dataId;
}
